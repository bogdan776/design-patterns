# Simple examples of design patterns

## Now you can find examples for next patterns:

```
Singleton (default, lazy, enum, lock, volatile, semaphore)
```
```
Prototype
```
```
Builder
```
```
Factory Method
```
```
Abstract Factory
```
```
Chain of responsibility
```
```
Command
```
```
Iterator
```
```
Momento
```
```
Mediator
```
```
Observer
```
```
Decorator
```
```
State
```
```
Facade
```
```
Strategy
```
```
Template Method
```
```
Flyweight
```
```
Proxy
```
```
Visitor
```
```
Adapter
```
```
Bridge
```
```
Composite
```
```
Interpreter
```
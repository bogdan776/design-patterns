package com.example.iterator;

import org.junit.Assert;
import org.junit.Test;

public class MyIteratorTest {

    @Test
    public void iteratorTest() {
        Array<String> array = new ArrayContainer<>();
        array.add("One");
        array.add("Two");
        array.add("Three");
        String[] expected = {"One", "Two", "Three"};
        String[] actual = new String[3];
        MyIterator<String> iterator = array.iterator();
        int i = 0;
        while (iterator.hasNext()) {
            actual[i++] = iterator.next();
        }
        Assert.assertArrayEquals(expected, actual);
    }

    @Test
    public void removeTest() {
        Array<Integer> array = new ArrayContainer<>();
        array.add(1);
        array.add(2);
        array.add(3);
        array.add(4);
        MyIterator<Integer> iterator = array.iterator();
        if (iterator.hasNext()) {
            iterator.next();
            iterator.next();
            iterator.remove();
        }
        Assert.assertEquals("[1, 3, 4]", array.toString());
    }

    @Test(expected = IllegalStateException.class)
    public void wrongRemoveTest() {
        Array<Double> array = new ArrayContainer<>();
        array.add(5.45);
        array.add(878.6);
        array.add(8.464);
        array.add(7.62);
        MyIterator<Double> iterator = array.iterator();
        iterator.remove();
    }
}

package com.example.iterator;

import org.junit.Assert;
import org.junit.Test;

public class ArrayTest {

    @Test
    public void testAdd() {
        Array<Integer> array = new ArrayContainer<>();
        Assert.assertEquals(0, array.size());
        array.add(1);
        array.add(2);
        Assert.assertEquals(2, array.size());
    }

    @Test
    public void testTestAdd() {
        Array<Integer> array = new ArrayContainer<>();
        array.add(1);
        array.add(2);
        array.add(3);
        array.add(4);
        Assert.assertEquals("[1, 2, 3, 4]", array.toString());
        array.add(2, 100);
        Assert.assertEquals("[1, 2, 100, 3, 4]", array.toString());
    }

    @Test
    public void testSet() {
        Array<Integer> array = new ArrayContainer<>();
        array.add(1);
        array.add(2);
        array.add(3);
        array.add(4);
        Assert.assertEquals("[1, 2, 3, 4]", array.toString());
        array.set(2, 100);
        Assert.assertEquals("[1, 2, 100, 4]", array.toString());
    }

    @Test
    public void testGet() {
        Array<Double> array = new ArrayContainer<>();
        array.add(1.);
        array.add(2.48);
        array.add(300.844);
        array.add(4.78);
        Assert.assertEquals(300.844, array.get(2), 0.01);
    }

    @Test
    public void testIndexOf() {
        Array<Double> array = new ArrayContainer<>();
        array.add(1.);
        array.add(2.48);
        array.add(300.844);
        array.add(4.78);
        Assert.assertEquals(1, array.indexOf(2.48));
    }

    @Test
    public void testRemove() {
        Array<String> array = new ArrayContainer<>();
        array.add("Java");
        array.add("Kotlin");
        array.add("JavaScript");
        array.add("Scala");
        Assert.assertEquals(4, array.size());
        array.remove(2);
        Assert.assertEquals(3, array.size());
    }
}

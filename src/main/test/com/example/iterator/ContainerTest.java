package com.example.iterator;

import org.junit.Assert;
import org.junit.Test;

public class ContainerTest {

    @Test
    public void testClear() {
        Array<String> array = new ArrayContainer<>();
        array.add("One");
        array.add("Two");
        array.add("Three");
        Assert.assertEquals(3, array.size());
        array.clear();
        Assert.assertEquals(0, array.size());
    }

    @Test
    public void testSize() {
        Array<Integer> array = new ArrayContainer<>();
        array.add(1);
        array.add(2);
        array.add(3);
        array.add(4);
        Assert.assertEquals(4, array.size());
    }

    @Test
    public void testToString() {
        Array<String> array = new ArrayContainer<>();
        array.add("Spring");
        array.add("String");
        array.add("Swing");
        Assert.assertEquals("[Spring, String, Swing]", array.toString());
    }
}

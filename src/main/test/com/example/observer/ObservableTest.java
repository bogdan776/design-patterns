package com.example.observer;

import org.junit.Assert;
import org.junit.Test;

public class ObservableTest {

    @Test
    public void testAddObserver() {
        NewsAgency observable = new NewsAgency();
        NewsChannel observer = new NewsChannel();
        observable.addObserver(observer);
        Assert.assertEquals(1, observable.countObservers());
    }

    @Test
    public void testNotifyObservers() {
        NewsAgency observable = new NewsAgency();
        NewsChannel observer = new NewsChannel();
        observable.addObserver(observer);
        Assert.assertEquals("No news yet", observer.toString());
        observable.addNews("You passed this test");
        String expected = "News:" + System.lineSeparator() + "1. You passed this test" + System.lineSeparator();
        Assert.assertEquals(expected, observer.toString());
    }

    @Test
    public void testNotifyObserversOfRemoval() {
        NewsAgency observable = new NewsAgency();
        NewsChannel observer = new NewsChannel();
        observable.addObserver(observer);
        observable.addNews("You passed this test", "Remove");
        String separator = System.lineSeparator();
        String expected = "News:" + separator + "1. You passed this test"
                + separator + "2. Remove" + separator;
        Assert.assertEquals(expected, observer.toString());
        observable.removeNews("Remove");
        expected = "News:" + separator + "1. You passed this test" + separator;
        Assert.assertEquals(expected, observer.toString());
    }

    @Test
    public void testDeleteObserver() {
        NewsAgency observable = new NewsAgency();
        NewsChannel observerOne = new NewsChannel();
        NewsChannel observerTwo = new NewsChannel();
        observable.addObserver(observerOne);
        observable.addObserver(observerTwo);
        Assert.assertEquals(2, observable.countObservers());
        observable.deleteObserver(observerTwo);
        Assert.assertEquals(1, observable.countObservers());
    }

    @Test
    public void testDeleteObservers() {
        NewsAgency observable = new NewsAgency();
        NewsChannel observerOne = new NewsChannel();
        NewsChannel observerTwo = new NewsChannel();
        observable.addObserver(observerOne);
        observable.addObserver(observerTwo);
        Assert.assertEquals(2, observable.countObservers());
        observable.deleteObservers();
        Assert.assertEquals(0, observable.countObservers());
    }
}

package com.example.abstractfactory.service;

import com.example.abstractfactory.dao.DaoFactory;
import com.example.abstractfactory.dao.DatabaseType;
import com.example.abstractfactory.entity.User;
import com.example.abstractfactory.exception.StorageException;
import org.junit.Assert;
import org.junit.Test;

public class UserServiceTest {

    private final UserService service = new UserService(DaoFactory.createDaoFactory(DatabaseType.ARRAY_LIST));

    @Test
    public void testCreate() throws StorageException {
        User user = new User();
        user.setEmail("new@yahoo.com");
        user.setPassword("password");
        user.setName("New user");
        int id = service.create(user);
        Assert.assertEquals(5, id);
    }

    @Test
    public void testLogin() throws StorageException {
        User user = service.login("1@gmail.com", "User01");
        Assert.assertEquals("One", user.getName());
    }

    @Test(expected = StorageException.class)
    public void testLoginWrongUsernameAndPassword() throws StorageException {
        User user = service.login("admin", "admin");
        Assert.assertNull(user);
    }

    @Test
    public void testDelete() {
        User user;
        try {
            user = service.login("1@gmail.com", "User01");
            Assert.assertTrue(service.delete(user.getId()));
        } catch (StorageException e) {
            Assert.assertNull(e.getMessage());
        }
    }
}
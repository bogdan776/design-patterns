package com.example.abstractfactory.service;

import com.example.abstractfactory.dao.DaoFactory;
import com.example.abstractfactory.dao.DatabaseType;
import com.example.abstractfactory.entity.Order;
import com.example.abstractfactory.entity.OrderStatus;
import com.example.abstractfactory.entity.Product;
import com.example.abstractfactory.exception.StorageException;
import com.example.util.Mapper;
import com.example.util.Reader;
import com.fasterxml.jackson.databind.type.TypeFactory;
import lombok.SneakyThrows;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class OrderServiceTest {

    private final OrderService service = new OrderService(DaoFactory.createDaoFactory(DatabaseType.ARRAY_LIST));

    @Test
    public void testCreate() throws StorageException {
        List<Product> products = new ArrayList<>();
        products.add(new Product("Headphones", 15.5));
        products.add(new Product("Sony Walkman", 500.));
        Order order = new Order();
        order.setUserId(0);
        order.setProducts(products);
        int i = service.create(order);
        Assert.assertEquals(11, i);
    }

    @Test
    public void testSelect() throws StorageException, IOException {
        Order order = service.select(10);
        String json = Reader.read("input-data/order.json");
        Order jsonOrder = Mapper.MAPPER.readValue(json, Order.class);
        Assert.assertEquals(jsonOrder, order);
    }

    @Test
    public void testSelectOrdersByUserId() throws StorageException {
        List<Order> orders = service.selectOrdersByUserId(0);
        List<Order> jsonOrder = findOrdersByUserIdFromJson(0);
        Assert.assertEquals(jsonOrder, orders);
    }

    @Test
    public void testUpdateStatus() throws StorageException {
        service.updateStatus(0, OrderStatus.CLOSED);
        Order order = service.select(0);
        Assert.assertEquals(OrderStatus.CLOSED, order.getOrderStatus());
    }

    @Test
    public void testDelete() throws StorageException {
        Assert.assertTrue(service.delete(0));
    }

    @Test(expected = StorageException.class)
    public void wrongTestDelete() throws StorageException {
        Assert.assertTrue(service.delete(25));
    }

    @SneakyThrows
    private List<Order> findOrdersByUserIdFromJson(Integer userId) {
        String json = Reader.read("input-data/orders.json");
        TypeFactory factory = Mapper.MAPPER.getTypeFactory();
        List<Order> orders = Mapper.MAPPER.readValue(json, factory.constructCollectionType(List.class, Order.class));
        return orders.stream().filter(o -> o.getUserId().equals(userId)).collect(Collectors.toList());
    }
}
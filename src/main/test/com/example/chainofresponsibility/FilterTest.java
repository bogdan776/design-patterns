package com.example.chainofresponsibility;

import org.junit.Assert;
import org.junit.Test;

public class FilterTest {

    @Test
    public void testCapsLookFilter() {
        Filter filter = new CapsLookFilter();
        String result = filter.filterManager("caps");
        Assert.assertEquals("CAPS", result);
    }

    @Test
    public void testExclamationMarkFilter() {
        Filter filter = new CapsLookFilter();
        Filter exclamationMarkFilter = new ExclamationMarkFilter();
        filter.setNext(exclamationMarkFilter);
        String result = filter.filterManager("Hello");
        Assert.assertEquals("HELLO!", result);
    }

    @Test
    public void testSmileFilter() {
        Filter filter = new CapsLookFilter();
        Filter smileFilter = new SmileFilter();
        filter.setNext(smileFilter);
        String result = filter.filterManager("hi^_^");
        Assert.assertEquals("HI^_^", result);
    }

    @Test
    public void testMirrorReflectionFilter() {
        Filter caps = new CapsLookFilter();
        Filter reverse = new MirrorReflectionFilter();
        caps.setNext(reverse);
        String result = caps.filterManager("hello");
        Assert.assertEquals("HELLO | OLLEH", result);
    }
}

package com.example.command;

import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

public class CommandTest {

    @Test
    public void testCreditingCommand() {
        Account account = new Account(5695, 4848.5, false);
        Receiver receiver = new Receiver(account);
        Client client = new Client(receiver);
        Command command = client.initCommand(CommandType.CREDITING);
        Invoker invoker = new Invoker(command);
        double balanceBefore = account.getBalance();
        invoker.invokeCommand();
        Assert.assertNotEquals(balanceBefore, account.getBalance());
    }

    @Test
    public void testWithdrawingCommand() {
        Account account = new Account(6855, 1250., false);
        Receiver receiver = new Receiver(account);
        Client client = new Client(receiver);
        Command withdrawing = client.initCommand(CommandType.WITHDRAWING);
        Invoker invoker = new Invoker(withdrawing);
        InputStream inputStreamStandard = System.in;
        System.setIn(new ByteArrayInputStream("250".getBytes()));
        invoker.invokeCommand();
        System.setIn(inputStreamStandard);
        Assert.assertEquals(1000., account.getBalance(), 0.01);
    }

    @Test
    public void testBlockingCommand() {
        Account account = new Account(468, 4648.8, true);
        Receiver receiver = new Receiver(account);
        Client client = new Client(receiver);
        Command command = client.initCommand(CommandType.BLOCKING);
        Invoker invoker = new Invoker(command);
        invoker.invokeCommand();
        Assert.assertFalse(account.isBlocked());
    }
}
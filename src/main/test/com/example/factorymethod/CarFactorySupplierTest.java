package com.example.factorymethod;

import com.example.builder.Car;
import org.junit.Assert;
import org.junit.Test;

public class CarFactorySupplierTest {

    @Test
    public void javaBuilderTest() {
        CarFactory factory = CarFactorySupplier.createFactory(CarFactoryType.JAVA_BUILDER);
        Car car = factory.build();
        Assert.assertEquals("Mercedes-Benz E200", car.getBrand() + " " + car.getModel());
    }

    @Test
    public void jsonParserTest() {
        CarFactory factory = CarFactorySupplier.createFactory(CarFactoryType.JSON_PARSER);
        Car car = factory.build();
        Assert.assertEquals("Mercedes-AMG GLE 53 4MATIC+", car.getBrand() + " " + car.getModel());
    }
}

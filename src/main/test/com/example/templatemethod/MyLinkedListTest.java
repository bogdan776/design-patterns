package com.example.templatemethod;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MyLinkedListTest {

    @Test
    public void testAddAll() {
        List<String> list = new ArrayList<>(Arrays.asList("One", "Two", "Three"));
        MyList<String> myList = new MyLinkedList<>();
        myList.addAll(list);
        Assert.assertEquals("[One, Two, Three]", myList.toString());
    }

    @Test
    public void testAdd() {
        MyList<Integer> myList = new MyLinkedList<>();
        myList.add(1);
        myList.add(2);
        myList.add(3);
        Assert.assertEquals("[1, 2, 3]", myList.toString());
    }

    @Test
    public void testGet() {
        MyList<Integer> myList = new MyLinkedList<>();
        myList.add(1);
        myList.add(2);
        myList.add(3);
        myList.add(4);
        Assert.assertEquals(new Integer(3), myList.get(2));
    }

    @Test
    public void testSize() {
        MyList<Integer> myList = new MyLinkedList<>();
        final int N = 150;
        for (int i = 0; i < N; i++) {
            myList.add(i);
        }
        Assert.assertEquals(N, myList.size());
    }
}

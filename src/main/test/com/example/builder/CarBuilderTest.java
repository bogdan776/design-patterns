package com.example.builder;

import com.example.util.Mapper;
import com.example.util.Reader;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

public class CarBuilderTest {

    @Test
    public void buildTest() {
        CarBuilder builder = new CarBuilderImpl();
        Car car = builder.setBrand("Mercedes-Benz")
                .setModel("G350d")
                .setEngine(createEngine())
                .setColor(Color.BLACK)
                .setDrive(Drive.AWD)
                .setMaxSpeed(250)
                .setPrice(97115.90)
                .setYear(2019)
                .build();
        Car carFromJson;
        try {
            carFromJson = getInstanceFromJsonFile();
            Assert.assertEquals(carFromJson, car);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    private Car getInstanceFromJsonFile() throws IOException {
        String json = Reader.read("input-data/mb_g350d.json");
        return Mapper.MAPPER.readValue(json, Car.class);
    }

    private Engine createEngine() {
        Engine engine = new Engine();
        engine.setFuelType(Engine.FuelType.DIESEL);
        engine.setPower(286);
        engine.setTransmission(Engine.Transmission.AT);
        engine.setFuelConsumption(9.71);
        return engine;
    }
}
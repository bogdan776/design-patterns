package com.example.singleton.semaphore;

import org.junit.Assert;
import org.junit.Test;

public class SemaphoreSingletonTest {

    @Test
    public void test() {
        try {
            SemaphoreSingleton singleton = SemaphoreSingleton.getInstance(0);
            singleton.addContext("Hello");
            addContext();
            Assert.assertEquals("Hello Singleton!", singleton.getContext());
        } catch (SingletonException e) {
            System.out.println(e.getMessage());
        }
    }

    @Test(expected = SingletonException.class)
    public void singletonExceptionTest() throws SingletonException {
        SemaphoreSingleton singleton = null;
        for (int i = 0; i < 11; i++) {
            singleton = SemaphoreSingleton.getInstance(i);
            singleton.addContext("Instance #" + i);
        }
        String context = singleton.getContext();
        Assert.assertNotEquals("Instance #10", context);
    }

    private void addContext() throws SingletonException {
        SemaphoreSingleton singleton = SemaphoreSingleton.getInstance(0);
        singleton.addContext(" Singleton!");
    }
}
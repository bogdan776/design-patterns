package com.example.singleton;

import org.junit.Assert;
import org.junit.Test;

public class SingletonTest {

    @Test
    public void initTest() {
        Singleton singleton = Singleton.getInstance();
        singleton.addContext("Hello");
        addContext();
        String context = singleton.getContext();
        Assert.assertEquals("Hello World!", context);
    }

    private void addContext() {
        Singleton singleton = Singleton.getInstance();
        singleton.addContext(" World!");
    }
}
package com.example.singleton;

import org.junit.Assert;
import org.junit.Test;

public class SingletonEnumTest {

    @Test
    public void test() {
        SingletonEnum singletonEnum = SingletonEnum.INSTANCE;
        singletonEnum.setCount(5);
        int count = singletonEnum.getCount();
        Assert.assertEquals(5, count);
    }

    @Test
    public void changeFieldInSingletonSeveralTimes() {
        firstChange();
        secondChange();
        thirdChange();
        SingletonEnum singletonEnum = SingletonEnum.INSTANCE;
        singletonEnum.increment();
        int count = singletonEnum.getCount();
        Assert.assertEquals(-3, count);
    }

    private void firstChange() {
        SingletonEnum singletonEnum = SingletonEnum.INSTANCE;
        singletonEnum.setCount(3);
    }

    private void secondChange() {
        SingletonEnum singletonEnum = SingletonEnum.INSTANCE;
        singletonEnum.increment();
        singletonEnum.increment();
    }

    private void thirdChange() {
        SingletonEnum singletonEnum = SingletonEnum.INSTANCE;
        singletonEnum.setCount(singletonEnum.getCount() - 10);
        singletonEnum.increment();
    }
}
package com.example.state;

import org.junit.Assert;
import org.junit.Test;

public class ActivityTest {

    @Test
    public void testNext() {
        Person person = new Person();
        person.setActivity(States.WORKING);
        Assert.assertEquals("Working", person.printState());
        person.nextState();
        Assert.assertEquals("Walking", person.printState());
    }

    @Test
    public void testPrevious() {
        Person person = new Person(States.SLEEPING);
        Assert.assertEquals("Sleeping", person.printState());
        person.previousState();
        Assert.assertEquals("Resting", person.printState());
    }
}
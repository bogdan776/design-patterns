package com.example.prototype.simple;

import org.junit.Assert;
import org.junit.Test;

public class BookClientTest {

    @Test
    public void test() {
        Book book = new Book(1L, "First note", "Author One", "Context first note");
        BookClient client = new BookClient(book);
        Book clone = client.cloneBook();
        Assert.assertEquals(book, clone);
    }
}
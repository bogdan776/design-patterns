package com.example.prototype.bookselling;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class IssueCacheClientTest {

    private final List<Issue> issues = Arrays.asList(
            new Book(16, "Joshua Bloch", "Effective Java", 2008),
            new Book(32, "J. K. Rowling", "Harry Potter and the Philosopher's Stone", 1997),
            new Magazine(64, "Forbes", 1, 1917),
            new Magazine(128, "Vogue", 5, 1893)
    );

    @Test
    public void testCloneElementById() {
        IssueCacheClient client = new IssueCacheClient(issues);
        Issue issue;
        try {
            issue = client.cloneElementById(128);
            Assert.assertEquals(issue, issues.get(3));
        } catch (CloneNotSupportedException e) {
            System.out.println(e.getMessage());
        }
    }

    @Test
    public void testCloneElements() {
        IssueCacheClient client = new IssueCacheClient(issues);
        List<Issue> issueClones;
        try {
            issueClones = client.cloneElements("Effective Java", 1997, 1917);
            List<Issue> expectedList = new ArrayList<>();
            expectedList.add(issues.get(0));
            expectedList.add(issues.get(1));
            expectedList.add(issues.get(2));
            Assert.assertEquals(expectedList, issueClones);
        } catch (CloneNotSupportedException e) {
            System.out.println(e.getMessage());
        }
    }
}
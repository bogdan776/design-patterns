package com.example.decorator;

import com.example.util.Reader;
import lombok.SneakyThrows;
import org.junit.Assert;
import org.junit.Test;

public class ChristmasTreeTest {

    @Test
    public void simpleChristmasTree() {
        ChristmasTree christmasTree = new ChristmasTreeImpl();
        Assert.assertEquals("Новогодняя елка. Не ёлка - елка!", christmasTree.decorate());
    }

    @SneakyThrows
    @Test
    public void christmasTreeToTheMaximum() {
        ChristmasTree christmasTree = new Garland(
                new ChristmasBubble(
                        new StarOfBethlehem(
                                new ChristmasTreeImpl()), "бабушкин шарик из коробки", "конфетки", "снежинки")
        );
        String expected = Reader.read("input-data/input_data_for_christmas_tree.txt");
        Assert.assertEquals(expected, christmasTree.decorate());
    }
}

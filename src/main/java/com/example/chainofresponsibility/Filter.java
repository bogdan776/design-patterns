package com.example.chainofresponsibility;

public abstract class Filter {

    private Filter next = null;

    public String filterManager(String context) {
        context = filter(context);
        if (next != null) {
            return next.filterManager(context);
        }
        return context;
    }

    public abstract String filter(String context);

    public void setNext(Filter next) {
        this.next = next;
    }
}

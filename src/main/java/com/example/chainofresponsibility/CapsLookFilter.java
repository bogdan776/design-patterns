package com.example.chainofresponsibility;

public class CapsLookFilter extends Filter {

    @Override
    public String filter(String context) {
        return context.toUpperCase();
    }
}

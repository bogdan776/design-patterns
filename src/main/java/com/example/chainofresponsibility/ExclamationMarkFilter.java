package com.example.chainofresponsibility;

public class ExclamationMarkFilter extends Filter {

    @Override
    public String filter(String context) {
        char lats = context.charAt(context.length() - 1);
        if ((int) lats != 33) {
            context = context.concat("!");
        }
        return context;
    }
}

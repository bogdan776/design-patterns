package com.example.chainofresponsibility;

public class SmileFilter extends Filter {

    @Override
    public String filter(String context) {
        String smile = "^_^";
        String result;
        if (context.length() > 3) {
            String l3 = context.substring(context.length() - 3);
            if (!smile.equals(l3)) {
                result = context.concat(smile);
            } else return context;
        } else if (!context.equals(smile)) {
            result = context.concat(smile);
        } else return context;
        return result;
    }
}

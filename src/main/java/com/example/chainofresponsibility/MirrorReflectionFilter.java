package com.example.chainofresponsibility;

public class MirrorReflectionFilter extends Filter {

    @Override
    public String filter(String context) {
        StringBuilder stringBuilder = new StringBuilder(context);
        return context.concat(" | " + stringBuilder.reverse().toString());
    }
}

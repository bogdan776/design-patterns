package com.example.chainofresponsibility;

public class Runner {

    public static void main(String[] args) {
        Filter caps = new CapsLookFilter();
        Filter exclamation = new ExclamationMarkFilter();
        Filter smile = new SmileFilter();
        Filter mirror = new MirrorReflectionFilter();

        /*
        chain
         */
        caps.setNext(exclamation);
        exclamation.setNext(smile);
        smile.setNext(mirror);

        String result = caps.filterManager("hello world");
        System.out.println(result);
    }
}

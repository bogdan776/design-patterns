package com.example.builder;

/**
 * AWD - all-wheel drive
 * FWD - Front-wheel drive
 * RWD - Rear-wheel drive
 */
public enum Drive {
    AWD, FWD, RWD
}

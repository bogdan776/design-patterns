package com.example.builder;

public class CarBuilderImpl implements CarBuilder {

    private Car car = new Car();

    @Override
    public CarBuilder setBrand(String brand) {
        car.setBrand(brand);
        return this;
    }

    @Override
    public CarBuilder setModel(String model) {
        car.setModel(model);
        return this;
    }

    @Override
    public CarBuilder setEngine(Engine engine) {
        car.setEngine(engine);
        return this;
    }

    @Override
    public CarBuilder setDrive(Drive drive) {
        car.setDrive(drive);
        return this;
    }

    @Override
    public CarBuilder setColor(Color color) {
        car.setColor(color);
        return this;
    }

    @Override
    public CarBuilder setMaxSpeed(Integer maxSpeed) {
        car.setMaxSpeed(maxSpeed);
        return this;
    }

    @Override
    public CarBuilder setPrice(Double price) {
        car.setPrice(price);
        return this;
    }


    @Override
    public CarBuilder setYear(Integer year) {
        car.setYear(year);
        return this;
    }

    @Override
    public Car build() {
        Car newCar = new Car(car);
        car = new Car();
        return newCar;
    }
}

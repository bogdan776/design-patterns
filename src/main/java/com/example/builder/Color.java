package com.example.builder;

public enum Color {
    WHITE, GRAY, BLACK, RED, PINK, ORANGE, YELLOW, GREEN
}

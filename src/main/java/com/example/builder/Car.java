package com.example.builder;

import com.example.util.Mapper;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;

@Data
@NoArgsConstructor
public class Car {

    private String brand;

    private String model;

    private Engine engine;

    private Drive drive;

    private Color color;

    private Integer maxSpeed;

    private Double price;

    private Integer year;

    public Car(Car car) {
        this.brand = car.brand;
        this.model = car.model;
        this.engine = car.engine;
        this.drive = car.drive;
        this.color = car.color;
        this.maxSpeed = car.maxSpeed;
        this.price = car.price;
        this.year = car.year;
    }

    @SneakyThrows
    @Override
    public String toString() {
        return Mapper.MAPPER.writeValueAsString(this);
    }
}

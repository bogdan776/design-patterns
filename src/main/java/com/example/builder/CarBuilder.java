package com.example.builder;

public interface CarBuilder {

    CarBuilder setBrand(String brand);

    CarBuilder setModel(String model);

    CarBuilder setEngine(Engine engine);

    CarBuilder setDrive(Drive drive);

    CarBuilder setColor(Color color);

    CarBuilder setMaxSpeed(Integer maxSpeed);

    CarBuilder setPrice(Double price);

    CarBuilder setYear(Integer year);

    Car build();
}

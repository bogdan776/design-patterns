package com.example.builder;

import lombok.Data;

@Data
public class Engine {

    private FuelType fuelType;

    private Integer power;

    private Transmission transmission;

    private Double fuelConsumption;

    public enum FuelType {
        DIESEL, GASOLINE, ELECTRICITY
    }

    public enum Transmission {
        AT, MT
    }
}

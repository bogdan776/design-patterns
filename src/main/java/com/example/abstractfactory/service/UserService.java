package com.example.abstractfactory.service;

import com.example.abstractfactory.dao.DaoFactory;
import com.example.abstractfactory.dao.UserDao;
import com.example.abstractfactory.entity.User;
import com.example.abstractfactory.exception.StorageException;

public class UserService {

    private final UserDao userDao;

    public UserService(DaoFactory daoFactory) {
        userDao = daoFactory.createUserDao();
    }

    public int create(User user) throws StorageException {
        return userDao.createUser(user);
    }

    public User login(String email, String password) throws StorageException {
        return userDao.login(email, password);
    }

    public boolean delete(Integer id) throws StorageException {
        return userDao.deleteUserById(id);
    }
}

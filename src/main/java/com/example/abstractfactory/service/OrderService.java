package com.example.abstractfactory.service;

import com.example.abstractfactory.dao.DaoFactory;
import com.example.abstractfactory.dao.OrderDao;
import com.example.abstractfactory.entity.Order;
import com.example.abstractfactory.entity.OrderStatus;
import com.example.abstractfactory.exception.StorageException;

import java.util.List;

public class OrderService {

    private final OrderDao orderDao;

    public OrderService(DaoFactory daoFactory) {
        orderDao = daoFactory.createOrderDao();
    }

    public int create(Order order) throws StorageException {
        return orderDao.createOrder(order);
    }

    public Order select(Integer id) throws StorageException {
        return orderDao.selectOrderById(id);
    }

    public List<Order> selectOrdersByUserId(Integer userId) throws StorageException {
        return orderDao.selectOrdersByUserId(userId);
    }

    public void updateStatus(Integer id, OrderStatus status) throws StorageException {
        orderDao.updateOrderStatus(id, status);
    }

    public boolean delete(Integer id) throws StorageException {
        return orderDao.deleteOrderById(id);
    }
}

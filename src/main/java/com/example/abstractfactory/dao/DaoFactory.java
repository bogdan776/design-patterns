package com.example.abstractfactory.dao;

import com.example.abstractfactory.dao.arraylist.ArrayListDaoFactory;
import com.example.abstractfactory.dao.mysql.MySqlDaoFactory;
import com.example.abstractfactory.dao.oracle.OracleDaoFactory;

public abstract class DaoFactory {

    public abstract OrderDao createOrderDao();

    public abstract UserDao createUserDao();

    public static DaoFactory createDaoFactory(DatabaseType databaseType) {
        switch (databaseType) {
            case MY_SQL:
                return new MySqlDaoFactory();
            case ORACLE:
                return new OracleDaoFactory();
            case ARRAY_LIST:
                return new ArrayListDaoFactory();
            default:
                throw new EnumConstantNotPresentException(DatabaseType.class, databaseType.name());
        }
    }
}

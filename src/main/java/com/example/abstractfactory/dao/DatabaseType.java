package com.example.abstractfactory.dao;

public enum DatabaseType {
    MY_SQL, ORACLE, ARRAY_LIST
}

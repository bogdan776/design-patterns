package com.example.abstractfactory.dao.mysql;

import com.example.abstractfactory.dao.DaoFactory;
import com.example.abstractfactory.dao.OrderDao;
import com.example.abstractfactory.dao.UserDao;

public class MySqlDaoFactory extends DaoFactory {

    @Override
    public OrderDao createOrderDao() {
        return new MySqlOrderDao();
    }

    @Override
    public UserDao createUserDao() {
        return new MySqlUserDao();
    }
}

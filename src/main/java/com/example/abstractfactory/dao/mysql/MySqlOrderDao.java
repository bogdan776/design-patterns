package com.example.abstractfactory.dao.mysql;

import com.example.abstractfactory.dao.OrderDao;
import com.example.abstractfactory.entity.Order;
import com.example.abstractfactory.entity.OrderStatus;

import java.util.List;

public class MySqlOrderDao implements OrderDao {

    @Override
    public int createOrder(Order order) {
        /*
        Awaiting implementation
         */
        return 0;
    }

    @Override
    public Order selectOrderById(Integer id) {
        /*
        Awaiting implementation
         */
        return null;
    }

    @Override
    public List<Order> selectOrdersByUserId(Integer userId) {
        /*
        Awaiting implementation
         */
        return null;
    }

    @Override
    public void updateOrderStatus(Integer id, OrderStatus status) {
        /*
        Awaiting implementation
         */
    }

    @Override
    public boolean deleteOrderById(Integer id) {
        /*
        Awaiting implementation
         */
        return false;
    }
}

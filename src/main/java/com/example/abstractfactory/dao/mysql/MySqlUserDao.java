package com.example.abstractfactory.dao.mysql;

import com.example.abstractfactory.dao.UserDao;
import com.example.abstractfactory.entity.User;

public class MySqlUserDao implements UserDao {

    @Override
    public int createUser(User user) {
        /*
        Awaiting implementation
         */
        return 0;
    }

    @Override
    public User login(String email, String password) {
        /*
        Awaiting implementation
         */
        return null;
    }

    @Override
    public boolean deleteUserById(Integer id) {
        /*
        Awaiting implementation
         */
        return false;
    }
}

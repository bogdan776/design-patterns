package com.example.abstractfactory.dao.arraylist;

import com.example.abstractfactory.dao.OrderDao;
import com.example.abstractfactory.entity.Order;
import com.example.abstractfactory.entity.OrderStatus;
import com.example.abstractfactory.exception.StorageException;
import com.example.abstractfactory.storage.OrderStorage;

import java.util.List;

public class ArrayListOrderDao implements OrderDao {

    private final OrderStorage storage = new OrderStorage();

    @Override
    public int createOrder(Order order) throws StorageException {
        return storage.createOrder(order);
    }

    @Override
    public Order selectOrderById(Integer id) throws StorageException {
        return storage.findOrderById(id);
    }

    @Override
    public List<Order> selectOrdersByUserId(Integer userId) throws StorageException {
        return storage.findOrdersByUserId(userId);
    }

    @Override
    public void updateOrderStatus(Integer id, OrderStatus status) throws StorageException {
        storage.updateOrderStatus(id, status);
    }

    @Override
    public boolean deleteOrderById(Integer id) throws StorageException {
        return storage.deleteOrder(id);
    }
}

package com.example.abstractfactory.dao.arraylist;

import com.example.abstractfactory.dao.UserDao;
import com.example.abstractfactory.entity.User;
import com.example.abstractfactory.exception.StorageException;
import com.example.abstractfactory.storage.UserStorage;

public class ArrayListUserDao implements UserDao {

    private final UserStorage storage = new UserStorage();

    @Override
    public int createUser(User user) throws StorageException {
        return storage.create(user);
    }

    @Override
    public User login(String email, String password) throws StorageException {
        return storage.login(email, password);
    }

    @Override
    public boolean deleteUserById(Integer id) throws StorageException {
        return storage.deleteUserById(id);
    }
}

package com.example.abstractfactory.dao.arraylist;

import com.example.abstractfactory.dao.DaoFactory;
import com.example.abstractfactory.dao.OrderDao;
import com.example.abstractfactory.dao.UserDao;

public class ArrayListDaoFactory extends DaoFactory {

    @Override
    public OrderDao createOrderDao() {
        return new ArrayListOrderDao();
    }

    @Override
    public UserDao createUserDao() {
        return new ArrayListUserDao();
    }
}

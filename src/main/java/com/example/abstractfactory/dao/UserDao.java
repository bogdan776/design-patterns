package com.example.abstractfactory.dao;

import com.example.abstractfactory.entity.User;
import com.example.abstractfactory.exception.StorageException;

public interface UserDao {

    int createUser(User user) throws StorageException;

    User login(String email, String password) throws StorageException;

    boolean deleteUserById(Integer id) throws StorageException;
}

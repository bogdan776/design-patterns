package com.example.abstractfactory.dao;

import com.example.abstractfactory.entity.Order;
import com.example.abstractfactory.entity.OrderStatus;
import com.example.abstractfactory.exception.StorageException;

import java.util.List;

public interface OrderDao {

    int createOrder(Order order) throws StorageException;

    Order selectOrderById(Integer id) throws StorageException;

    List<Order> selectOrdersByUserId(Integer userId) throws StorageException;

    void updateOrderStatus(Integer id, OrderStatus status) throws StorageException;

    boolean deleteOrderById(Integer id) throws StorageException;
}

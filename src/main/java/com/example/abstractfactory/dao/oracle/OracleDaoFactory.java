package com.example.abstractfactory.dao.oracle;

import com.example.abstractfactory.dao.DaoFactory;
import com.example.abstractfactory.dao.OrderDao;
import com.example.abstractfactory.dao.UserDao;

public class OracleDaoFactory extends DaoFactory {

    @Override
    public OrderDao createOrderDao() {
        return new OracleOrderDao();
    }

    @Override
    public UserDao createUserDao() {
        return new OracleUserDao();
    }
}

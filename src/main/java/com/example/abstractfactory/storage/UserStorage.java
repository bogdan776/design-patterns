package com.example.abstractfactory.storage;

import com.example.abstractfactory.entity.User;
import com.example.abstractfactory.exception.StorageException;
import com.example.util.Mapper;
import com.example.util.Reader;
import com.fasterxml.jackson.databind.type.TypeFactory;
import lombok.SneakyThrows;
import org.apache.commons.codec.digest.DigestUtils;

import java.util.ArrayList;
import java.util.List;

public class UserStorage {

    private final List<User> users;

    {
        users = new ArrayList<>();
        init();
    }

    public int create(User user) throws StorageException {
        if (users.stream().noneMatch(u -> u.getEmail().equals(user.getEmail()))) {
            int id = users.size();
            user.setId(id);
            String password = DigestUtils.sha256Hex(user.getPassword());
            user.setPassword(password);
            users.add(user);
            return id;
        } else throw new StorageException("Order already exists");
    }

    public User login(String email, String password) throws StorageException {
        User user = users.stream().filter(u -> u.getEmail().equals(email)).findFirst().orElseThrow(StorageException::new);
        String passwordSha256Hex = DigestUtils.sha256Hex(password);
        if (user.getPassword().equals(passwordSha256Hex)) {
            return user;
        } else throw new StorageException("Incorrect password");
    }

    public boolean deleteUserById(Integer id) throws StorageException {
        if (users.removeIf(u -> u.getId().equals(id))) {
            return true;
        } else throw new StorageException("User not found");
    }

    @SneakyThrows
    private void init() {
        String json = Reader.read("input-data/users.json");
        TypeFactory typeFactory = Mapper.MAPPER.getTypeFactory();
        users.addAll(Mapper.MAPPER.readValue(json, typeFactory.constructCollectionType(List.class, User.class)));
    }
}

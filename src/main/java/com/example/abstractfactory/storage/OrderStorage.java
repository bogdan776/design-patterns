package com.example.abstractfactory.storage;

import com.example.abstractfactory.entity.Order;
import com.example.abstractfactory.entity.OrderStatus;
import com.example.abstractfactory.exception.StorageException;
import com.example.util.Mapper;
import com.example.util.Reader;
import com.fasterxml.jackson.databind.type.TypeFactory;
import lombok.SneakyThrows;

import java.util.ArrayList;
import java.util.List;

public class OrderStorage {

    private final List<Order> orders;

    {
        orders = new ArrayList<>();
        init();
    }

    public int createOrder(Order order) throws StorageException {
        if (orders.stream().noneMatch(o -> o.equals(order))) {
            int id = orders.size();
            order.setId(id);
            order.setOrderStatus(OrderStatus.NEW);
            orders.add(order);
            return id;
        } else throw new StorageException("Order already exists");
    }

    public Order findOrderById(Integer id) throws StorageException {
        return orders.stream().filter(order -> order.getId().equals(id)).findFirst().orElseThrow(StorageException::new);
    }

    public List<Order> findOrdersByUserId(Integer userId) throws StorageException {
        List<Order> usersOrders = new ArrayList<>();
        orders.stream().filter(o -> o.getUserId().equals(userId)).forEach(usersOrders::add);
        if (usersOrders.isEmpty()) {
            throw new StorageException("User has no orders yet");
        }
        return usersOrders;
    }

    public void updateOrderStatus(Integer id, OrderStatus status) throws StorageException {
        findOrderById(id).setOrderStatus(status);
    }

    public boolean deleteOrder(Integer id) throws StorageException {
        if (orders.removeIf(o -> o.getId().equals(id))) {
            return true;
        } else throw new StorageException("Order not found");
    }

    @SneakyThrows
    private void init() {
        String json = Reader.read("input-data/orders.json");
        TypeFactory typeFactory = Mapper.MAPPER.getTypeFactory();
        orders.addAll(Mapper.MAPPER.readValue(json, typeFactory.constructCollectionType(List.class, Order.class)));
    }
}

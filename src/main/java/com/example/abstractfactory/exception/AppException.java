package com.example.abstractfactory.exception;

public class AppException extends Exception {

    public AppException() {
        super();
    }

    public AppException(String message) {
        super(message);
    }
}

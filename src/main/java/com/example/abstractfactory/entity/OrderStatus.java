package com.example.abstractfactory.entity;

public enum OrderStatus {
    NEW, PROCESSING, CLOSED
}

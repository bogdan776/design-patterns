package com.example.abstractfactory.entity;

import com.example.util.Mapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Order {

    private Integer id;

    private Integer userId;

    private List<Product> products;

    private OrderStatus orderStatus;

    @SneakyThrows
    @Override
    public String toString() {
        return Mapper.MAPPER.writeValueAsString(this);
    }
}

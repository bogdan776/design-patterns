package com.example.abstractfactory.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
public class User {

    private Integer id;

    private String name;

    private String email;

    private String password;
}

package com.example.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Writer {

    public static void write(Path path, String context) throws IOException {
        File file = path.toFile();
        if (!file.exists()) {
            Files.createFile(path);
        }
        Files.write(path, context.getBytes(StandardCharsets.UTF_8), StandardOpenOption.APPEND);
    }

    public static void createDirectory(Path path) throws IOException {
        File file = path.toFile();
        if (!file.isDirectory()) {
            Files.createDirectory(path);
        }
    }
}

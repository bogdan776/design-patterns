package com.example.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * In some classes method toString returns
 * a string representation of the object in JSON format
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Mapper {

    public static final ObjectMapper MAPPER = new ObjectMapper();
}

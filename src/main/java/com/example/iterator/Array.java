package com.example.iterator;

public interface Array<T> extends Container<T> {

    void add(T element);

    void add(int index, T element);

    void set(int index, T element);

    T get(int index);

    int indexOf(T element);

    void remove(int index);
}

package com.example.iterator;

import lombok.Setter;

import java.util.Arrays;
import java.util.NoSuchElementException;

@Setter
public class ArrayContainer<T> implements Array<T> {

    private static final Object[] EMPTY_ELEMENT_DATA = {};

    private Object[] elementData;

    public ArrayContainer() {
        elementData = EMPTY_ELEMENT_DATA;
    }

    @Override
    public void add(T element) {
        setElementData(addElementToArray(elementData.length, element));
    }

    @Override
    public void add(int index, T element) {
        rangeCheck(index);
        setElementData(addElementToArray(index, element));
    }

    @Override
    public void set(int index, T element) {
        rangeCheck(index);
        elementData[index] = element;
    }

    @Override
    @SuppressWarnings("unchecked")
    public T get(int index) {
        if (index < size()) {
            return (T) elementData[index];
        } else throw new IndexOutOfBoundsException();
    }

    @Override
    public int indexOf(T element) {
        int index = 0;
        for (Object o : elementData) {
            if (element.equals(o)) {
                break;
            }
            index++;
        }
        return index;
    }

    @Override
    public void remove(int index) {
        rangeCheck(index);
        Object[] nArray = new Object[elementData.length - 1];
        System.arraycopy(elementData, 0, nArray, 0, index);
        System.arraycopy(elementData, index + 1, nArray, index, nArray.length - index);
        setElementData(nArray);
    }

    @Override
    public void clear() {
        elementData = EMPTY_ELEMENT_DATA;
    }

    @Override
    public int size() {
        return elementData.length;
    }

    @Override
    public MyIterator<T> iterator() {
        return new MyIterator<T>() {

            private int cursor = 0;

            private boolean nextWasUsed = false;

            @Override
            public boolean hasNext() {
                return cursor < size();
            }

            @Override
            @SuppressWarnings("unchecked")
            public T next() {
                if (hasNext()) {
                    nextWasUsed = true;
                    return (T) elementData[cursor++];
                } else throw new NoSuchElementException();
            }

            @Override
            public void remove() {
                if (nextWasUsed) {
                    ArrayContainer.this.remove(--cursor);
                    nextWasUsed = false;
                } else throw new IllegalStateException();
            }
        };
    }

    @Override
    public String toString() {
        return Arrays.toString(elementData);
    }

    private Object[] addElementToArray(int index, T element) {
        Object[] nArray = new Object[elementData.length + 1];
        System.arraycopy(elementData, 0, nArray, 0, index);
        nArray[index] = element;
        System.arraycopy(elementData, index, nArray, index + 1, elementData.length - index);
        return nArray;
    }

    private void rangeCheck(int index) {
        if (index >= size() || index < 0) {
            throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size());
        }
    }
}

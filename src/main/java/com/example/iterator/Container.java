package com.example.iterator;

public interface Container<T> extends MyIterable<T> {

    void clear();

    int size();

    String toString();

    @Override
    MyIterator<T> iterator();
}

package com.example.iterator;

public interface MyIterable<T> {

    MyIterator<T> iterator();
}

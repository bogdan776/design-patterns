package com.example.iterator;

public interface MyIterator<E> {

    boolean hasNext();

    E next();

    void remove();
}

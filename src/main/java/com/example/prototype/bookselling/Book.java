package com.example.prototype.bookselling;

import com.example.util.Mapper;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@Data
public class Book extends Issue {

    private String author;

    public Book(Integer id, String author, String title, Integer year) {
        super(id, title, year);
        this.author = author;
    }

    @SneakyThrows
    @Override
    public String toString() {
        return Mapper.MAPPER.writeValueAsString(this);
    }
}

package com.example.prototype.bookselling;

import java.util.List;

public interface Client<T> {

    T cloneElementById(Integer id) throws CloneNotSupportedException;

    List<T> cloneElements(Object... params) throws CloneNotSupportedException;
}

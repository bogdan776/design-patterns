package com.example.prototype.bookselling;

import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
public class IssueCacheClient implements Client<Issue> {

    private final List<Issue> cache;

    @Override
    public Issue cloneElementById(Integer id) throws CloneNotSupportedException {
        for (Issue issue : cache) {
            if (issue.getId().equals(id)) {
                return (Issue) issue.clone();
            }
        }
        throw new IllegalArgumentException("Illegal ID: " + id);
    }

    @Override
    public List<Issue> cloneElements(Object... params) throws CloneNotSupportedException {
        List<Issue> issueList = new ArrayList<>();
        for (Object parameter : params) {
            for (Issue issue : cache) {
                if (parameter instanceof String) {
                    if (issue.getTitle().equals(parameter)) {
                        issueList.add((Issue) issue.clone());
                    }
                }
                if (parameter instanceof Integer) {
                    if (issue.getId().equals(parameter) || issue.getYear().equals(parameter)) {
                        issueList.add((Issue) issue.clone());
                    }
                }
            }
        }
        return issueList;
    }
}

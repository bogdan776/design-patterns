package com.example.prototype.bookselling;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public abstract class Issue implements Cloneable {

    private Integer id;

    private String title;

    private Integer year;

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}

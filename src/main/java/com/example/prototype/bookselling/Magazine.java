package com.example.prototype.bookselling;

import com.example.util.Mapper;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@Data
public class Magazine extends Issue {

    private Integer number;

    public Magazine(Integer id, String title, Integer number, Integer year) {
        super(id, title, year);
        this.number = number;
    }

    @SneakyThrows
    @Override
    public String toString() {
        return Mapper.MAPPER.writeValueAsString(this);
    }
}

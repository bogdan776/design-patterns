package com.example.prototype.simple;

import lombok.AllArgsConstructor;
import lombok.Data;


@AllArgsConstructor
@Data
public class BookClient {

    private Book book;

    public Book cloneBook() {
        return book.copy();
    }
}

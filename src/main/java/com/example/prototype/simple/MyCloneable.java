package com.example.prototype.simple;

public interface MyCloneable<T> {

    T copy();
}

package com.example.prototype.simple;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Book implements MyCloneable<Book> {

    private Long id;

    private String title;

    private String author;

    private String context;

    @Override
    public Book copy() {
        return new Book(id, title, author, context);
    }
}

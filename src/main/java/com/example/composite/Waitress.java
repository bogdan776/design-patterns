package com.example.composite;

import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;

@Log4j2
@AllArgsConstructor
public class Waitress implements Employee {

    private final String name;

    @Override
    public void work() {
        log.info("Good evening! My name is " + name + "! What would you like to eat or drink?");
    }
}

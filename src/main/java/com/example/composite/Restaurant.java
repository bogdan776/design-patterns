package com.example.composite;

public class Restaurant {

    public static void main(String[] args) {
        Employee cook = new Cook();
        Employee waitress = new Waitress("Katya");
        Employee courier = new Courier("Bogdan's Restaurant");
        Team team = new Team(cook, waitress, courier);
        team.addEmployee(new Waitress("Nastya"));
        team.work();
    }
}

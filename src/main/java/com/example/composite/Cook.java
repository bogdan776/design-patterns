package com.example.composite;

import lombok.Getter;
import lombok.extern.log4j.Log4j2;

import java.util.Random;

@Log4j2
@Getter
public class Cook implements Employee {

    @Override
    public void work() {
        Random random = new Random();
        int no = random.ints(0, 1 << 16).iterator().nextInt();
        log.info("Order No. " + no + " is ready");
    }
}

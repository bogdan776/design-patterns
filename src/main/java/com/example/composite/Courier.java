package com.example.composite;

import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;

@Log4j2
@AllArgsConstructor
public class Courier implements Employee {

    private final String companyName;

    @Override
    public void work() {
        log.info("Hello! I am from " + companyName + ". I brought your order!");
    }
}

package com.example.composite;

import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Log4j2
@NoArgsConstructor
public class Team {

    private final List<Employee> employees = new ArrayList<>();

    public Team(Employee... employees) {
        if (this.employees.addAll(Arrays.asList(employees)))
            log.info("all employees added");
    }

    public void addEmployee(Employee employee) {
        employees.add(employee);
    }

    public void removeEmployee(Employee employee) {
        employees.remove(employee);
    }

    public void work() {
        log.info("the team began to work");
        employees.forEach(Employee::work);
        log.info("the team finished work");
    }
}

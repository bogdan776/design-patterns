package com.example.state;

public class Walking implements Activity<Person> {

    @Override
    public String execute() {
        return "The park is so beautiful!";
    }

    @Override
    public void next(Person person) {
        person.setActivity(States.RESTING);
    }

    @Override
    public void previous(Person person) {
        person.setActivity(States.WORKING);
    }
}

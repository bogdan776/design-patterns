package com.example.state;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Person {

    private Activity<Person> activity;

    public String execute() {
        return activity.execute();
    }

    public void nextState() {
        activity.next(this);
    }

    public void previousState() {
        activity.previous(this);
    }

    public String printState() {
        return activity.printState();
    }
}

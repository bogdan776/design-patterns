package com.example.state;

import lombok.SneakyThrows;

public class StateDemo {

    @SneakyThrows
    public static void main(String[] args) {
        Person person = new Person(States.WALKING);
        for (int i = 0; i < 6; i++) {
            print(person.printState());
            print(person.execute());
            person.nextState();
            Thread.sleep(1000);
        }
        for (int i = 0; i < 6; i++) {
            print(person.printState());
            print(person.execute());
            person.previousState();
            Thread.sleep(1000);
        }
    }

    private static void print(String str) {
        System.out.println(str);
    }
}

package com.example.state;

public class Eating implements Activity<Person> {

    @Override
    public String execute() {
        return "Would you like fried potatoes?";
    }

    @Override
    public void next(Person person) {
        person.setActivity(States.WORKING);
    }

    @Override
    public void previous(Person person) {
        person.setActivity(States.SLEEPING);
    }
}

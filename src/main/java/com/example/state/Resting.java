package com.example.state;

public class Resting implements Activity<Person> {

    @Override
    public String execute() {
        return "What are we going to watch a tutorial from Indians or Anime?";
    }

    @Override
    public void next(Person person) {
        person.setActivity(States.SLEEPING);
    }

    @Override
    public void previous(Person person) {
        person.setActivity(States.WALKING);
    }
}

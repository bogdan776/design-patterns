package com.example.state;

public class Sleeping implements Activity<Person> {

    @Override
    public String execute() {
        return "Can't you fall asleep? You forgot to count the lambs!";
    }

    @Override
    public void next(Person person) {
        person.setActivity(States.EATING);
    }

    @Override
    public void previous(Person person) {
        person.setActivity(States.RESTING);
    }
}

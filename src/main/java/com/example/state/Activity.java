package com.example.state;

public interface Activity<T> {

    String execute();

    void next(T t);

    void previous(T t);

    default String printState() {
        return this.getClass().getSimpleName();
    }
}

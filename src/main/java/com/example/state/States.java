package com.example.state;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class States {

    public static final Activity<Person> SLEEPING = new Sleeping();

    public static final Activity<Person> RESTING = new Resting();

    public static final Activity<Person> WALKING = new Walking();

    public static final Activity<Person> WORKING = new Working();

    public static final Activity<Person> EATING = new Eating();
}

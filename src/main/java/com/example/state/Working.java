package com.example.state;

public class Working implements Activity<Person> {

    @Override
    public String execute() {
        return "Your code is very good";
    }

    @Override
    public void next(Person person) {
        person.setActivity(States.WALKING);
    }

    @Override
    public void previous(Person person) {
        person.setActivity(States.EATING);
    }
}

package com.example.visitor;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public abstract class Renting implements Visitable {

    private int term;

    private double price;

    public abstract double bill();
}

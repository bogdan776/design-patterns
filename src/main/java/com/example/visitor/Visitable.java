package com.example.visitor;

public interface Visitable {

    void accept(Visitor visitor);
}

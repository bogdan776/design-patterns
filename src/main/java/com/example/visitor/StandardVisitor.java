package com.example.visitor;

import java.util.Random;

public class StandardVisitor implements Visitor {

    @Override
    public void visit(DVDRenting service) {
        service.setNumberDisks(service.getNumberDisks() - 2);
        System.out.println("Standard DVD renting service");
    }

    @Override
    public void visit(ApplianceRenting service) {
        service.setPrice(service.getPrice() + new Random().nextInt(50));
        System.out.println("Standard Appliance renting service");
    }
}

package com.example.visitor;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(callSuper = true)
public class DVDRenting extends Renting {

    private int numberDisks;

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public double bill() {
        return super.getTerm() * (numberDisks * super.getPrice());
    }
}

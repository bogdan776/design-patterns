package com.example.visitor;

public class DiscountVisitor implements Visitor {

    @Override
    public void visit(DVDRenting service) {
        service.setNumberDisks(service.getNumberDisks() - 1);
        System.out.println("Discount DVD renting service");
    }

    @Override
    public void visit(ApplianceRenting service) {
        service.setPrice(service.getPrice() - Math.random());
        System.out.println("Discount Appliance renting service");
    }
}

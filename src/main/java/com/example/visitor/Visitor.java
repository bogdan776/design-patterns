package com.example.visitor;

public interface Visitor {

    void visit(DVDRenting service);

    void visit(ApplianceRenting service);
}

package com.example.visitor;

public class Client {

    public static void main(String[] args) {
        ApplianceRenting applianceRenting = new ApplianceRenting();
        applianceRenting.setPrice(15.55);
        applianceRenting.setTerm(2);
        applianceRenting.setDelivery(true);
        applianceRenting.accept(new StandardVisitor());
        System.out.println(applianceRenting);
        System.out.println("total: " + applianceRenting.bill() + "$");

        DVDRenting dvdRenting = new DVDRenting();
        dvdRenting.setNumberDisks(5);
        dvdRenting.setTerm(3);
        dvdRenting.setPrice(0.15);
        dvdRenting.accept(new DiscountVisitor());
        System.out.println(dvdRenting);
        System.out.println("total: " + Math.round(dvdRenting.bill()) + "$");
    }
}

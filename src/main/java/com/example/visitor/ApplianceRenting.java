package com.example.visitor;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(callSuper = true)
public class ApplianceRenting extends Renting {

    private boolean delivery = false;

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public double bill() {
        if (delivery)
            return (super.getTerm() * super.getPrice()) * 2;
        else return super.getTerm() * super.getPrice();
    }
}

package com.example.decorator;

public interface ChristmasTree {

    String decorate();
}

package com.example.decorator;

public class Garland extends TreeDecorator {

    public Garland(ChristmasTree christmasTree) {
        super(christmasTree);
    }

    @Override
    public String decorate() {
        return super.decorate() + " " + hangGarlandOnTheTree();
    }

    private String hangGarlandOnTheTree() {
        return "Раз, два, три! Ёлочка, гори!";
    }
}

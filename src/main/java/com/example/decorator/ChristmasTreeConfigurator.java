package com.example.decorator;

public class ChristmasTreeConfigurator {

    public static void main(String[] args) {
        ChristmasTree christmasTree = new Garland(
                new StarOfBethlehem(
                        new ChristmasBubble(
                                new ChristmasTreeImpl(), "бабушкин шарик из коробки", "конфетки")));
        System.out.println(christmasTree.decorate());
    }
}

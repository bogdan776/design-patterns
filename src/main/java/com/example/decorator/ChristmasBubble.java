package com.example.decorator;

public class ChristmasBubble extends TreeDecorator {

    private final String[] bubbles;

    public ChristmasBubble(ChristmasTree christmasTree, String... bubbles) {
        super(christmasTree);
        this.bubbles = bubbles;
    }

    @Override
    public String decorate() {
        return super.decorate() + " " + hangBubblesOnChristmasTree();
    }

    private String hangBubblesOnChristmasTree() {
        if (bubbles.length == 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Теперь на елке есть игрушки: ");
        for (String bubble : bubbles) {
            sb.append(bubble).append(", ");
        }
        sb.delete((sb.length() - 2), sb.length());
        sb.append(".");
        return sb.toString();
    }
}

package com.example.decorator;

public class ChristmasTreeImpl implements ChristmasTree {

    @Override
    public String decorate() {
        return "Новогодняя елка. Не ёлка - елка!";
    }
}

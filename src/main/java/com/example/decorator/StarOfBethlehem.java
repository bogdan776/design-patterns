package com.example.decorator;

public class StarOfBethlehem extends TreeDecorator {

    public StarOfBethlehem(ChristmasTree christmasTree) {
        super(christmasTree);
    }

    @Override
    public String decorate() {
        return super.decorate() + " " + crownTheTreeWithStar();
    }

    private String crownTheTreeWithStar() {
        return "Теперь и звезда на елке есть!";
    }
}

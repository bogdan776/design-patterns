package com.example.decorator;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class TreeDecorator implements ChristmasTree {

    private final ChristmasTree christmasTree;

    @Override
    public String decorate() {
        return christmasTree.decorate();
    }
}

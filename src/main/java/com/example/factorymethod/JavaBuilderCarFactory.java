package com.example.factorymethod;

import com.example.builder.Car;
import com.example.builder.CarBuilderImpl;
import com.example.builder.Color;
import com.example.builder.Drive;
import com.example.builder.Engine;

public class JavaBuilderCarFactory implements CarFactory {

    @Override
    public Car build() {
        return new CarBuilderImpl()
                .setBrand("Mercedes-Benz").setModel("E200")
                .setEngine(engine()).setDrive(Drive.AWD)
                .setColor(Color.WHITE).setMaxSpeed(215)
                .setPrice(49385.00).setYear(2020)
                .build();
    }

    private Engine engine() {
        Engine engine = new Engine();
        engine.setFuelType(Engine.FuelType.GASOLINE);
        engine.setPower(211);
        engine.setTransmission(Engine.Transmission.AT);
        engine.setFuelConsumption(6.61);
        return engine;
    }
}

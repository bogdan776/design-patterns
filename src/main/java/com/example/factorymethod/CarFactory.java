package com.example.factorymethod;

import com.example.builder.Car;

/**
 * For example, we have a car factory,
 * for example - Daimler AG. And our factory should build cars.
 * In this example, we’ll look at getting an instance using
 * the builder pattern and getting an instance using a parsing JSON file.
 */
public interface CarFactory {

    Car build();
}

package com.example.factorymethod;

import com.example.builder.Car;
import com.example.util.Mapper;
import com.example.util.Reader;

import java.io.IOException;

public class JsonParserCarFactory implements CarFactory {

    @Override
    public Car build() {
        String json;
        Car car = null;
        try {
            json = Reader.read("input-data/mb_gle53.json");
            car = Mapper.MAPPER.readValue(json, Car.class);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return car;
    }
}

package com.example.factorymethod;

public class CarFactorySupplier {

    public static CarFactory createFactory(CarFactoryType type) {
        switch (type) {
            case JAVA_BUILDER:
                return new JavaBuilderCarFactory();
            case JSON_PARSER:
                return new JsonParserCarFactory();
            default:
                throw new EnumConstantNotPresentException(CarFactoryType.class, type.name());
        }
    }
}

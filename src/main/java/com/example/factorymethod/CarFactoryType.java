package com.example.factorymethod;

public enum CarFactoryType {

    JAVA_BUILDER, JSON_PARSER
}

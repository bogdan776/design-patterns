package com.example.interpreter;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@AllArgsConstructor
@Setter
@Getter
public class TerminalExpression implements Expression<String> {

    private String data;

    @Override
    public boolean interpret(String context) {
        Matcher matcher = Pattern.compile("(?Umi)" + context).matcher(data);
        return matcher.find();
    }
}

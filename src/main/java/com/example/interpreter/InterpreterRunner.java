package com.example.interpreter;

public class InterpreterRunner {

    public static void main(String[] args) {
        Expression<String> and = and();
        Expression<String> or = or();
        System.out.println("Там везде был Карл? " + and.interpret("Карл"));
        System.out.println("Кто-то ехал? " + or.interpret("ехал"));
    }

    private static Expression<String> and() {
        Expression<String> expression1 = new TerminalExpression("Карл у Клары украл кораллы");
        Expression<String> expression2 = new TerminalExpression("Клара у Карла украла кларнет");
        return new AndExpression(expression1, expression2);
    }

    private static Expression<String> or() {
        Expression<String> expression1 = new TerminalExpression("Шла Саша по шоссе и сосала сушку");
        Expression<String> expression2 = new TerminalExpression("Ехал Грека через реку, видит Грека - в реке рак");
        return new OrExpression(expression1, expression2);
    }
}

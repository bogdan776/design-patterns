package com.example.interpreter;

public interface Expression<T> {

    boolean interpret(T context);
}

package com.example.interpreter;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class AndExpression implements Expression<String> {

    private Expression<String> expression1;
    private Expression<String> expression2;

    @Override
    public boolean interpret(String context) {
        return expression1.interpret(context) && expression2.interpret(context);
    }
}

package com.example.command;

import lombok.RequiredArgsConstructor;

import java.util.Scanner;

@RequiredArgsConstructor
public class Receiver {

    private final Account account;

    private static final double INTEREST_RATE = 6.3;

    public void action(CommandType commandType) {
        switch (commandType) {
            case CREDITING:
                if (account.isBlocked()) {
                    System.out.println("Sorry, the account #" + account.getId() + " is blocked");
                } else {
                    double balance = account.getBalance();
                    balance *= (INTEREST_RATE * 0.01);
                    account.setBalance(balance);
                    System.out.println("Crediting is performed with " + INTEREST_RATE + "% interest rate");
                }
                break;
            case WITHDRAWING:
                if (account.isBlocked()) {
                    System.out.println("Sorry, the account #" + account.getId() + " is blocked");
                } else {
                    double balance = account.getBalance();
                    double amount = ATMTerminal();
                    balance -= amount;
                    account.setBalance(balance);
                    System.out.println(amount + " is withdrawn from the account");
                }
                break;
            case BLOCKING:
                if (account.isBlocked()) {
                    account.setBlocked(false);
                    System.out.println("The account #" + account.getId() + " is unblocked");
                } else {
                    account.setBlocked(true);
                    System.out.println("The account #" + account.getId() + " is blocked");
                }
                break;
        }
    }

    private double ATMTerminal() {
        double amount;
        try (Scanner scanner = new Scanner(System.in)) {
            System.out.print("Enter amount: ");
            String input = scanner.nextLine();
            try {
                amount = Double.parseDouble(input);
            } catch (NumberFormatException ex) {
                amount = 0.0;
            }
        }
        return amount;
    }
}

package com.example.command;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class Client {

    private final Receiver receiver;

    public Command initCommand(CommandType commandType) {
        Command command = null;

        switch (commandType) {
            case CREDITING:
                command = new CreditingCommand(receiver);
                break;
            case WITHDRAWING:
                command = new WithdrawingCommand(receiver);
                break;
            case BLOCKING:
                command = new BlockingCommand(receiver);
                break;
        }
        return command;
    }
}

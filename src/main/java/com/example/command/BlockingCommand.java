package com.example.command;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class BlockingCommand implements Command {

    private final Receiver receiver;

    @Override
    public void execute() {
        receiver.action(CommandType.BLOCKING);
    }
}

package com.example.command;

public enum CommandType {

    CREDITING, WITHDRAWING, BLOCKING
}

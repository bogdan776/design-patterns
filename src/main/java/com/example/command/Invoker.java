package com.example.command;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class Invoker {

    private final Command command;

    public void invokeCommand() {
        command.execute();
    }
}

package com.example.command;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class CreditingCommand implements Command {

    private final Receiver receiver;

    @Override
    public void execute() {
        receiver.action(CommandType.CREDITING);
    }
}

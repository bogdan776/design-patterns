package com.example.command;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Account {

    private int id;

    private double balance;

    private boolean blocked;
}

package com.example.command;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class WithdrawingCommand implements Command {

    private final Receiver receiver;

    @Override
    public void execute() {
        receiver.action(CommandType.WITHDRAWING);
    }
}

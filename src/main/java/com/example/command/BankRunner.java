package com.example.command;

public class BankRunner {

    public static void main(String[] args) {
        Account account = new Account(15, 763.5, false);
        Receiver receiver = new Receiver(account);
        Client client = new Client(receiver);

        Command crediting = client.initCommand(CommandType.CREDITING);
        Invoker invokerCrediting = new Invoker(crediting);
        invokerCrediting.invokeCommand();

        Command withdrawing = client.initCommand(CommandType.WITHDRAWING);
        Invoker invokerWithdrawing = new Invoker(withdrawing);
        invokerWithdrawing.invokeCommand();

        Command blocking = client.initCommand(CommandType.BLOCKING);
        Invoker invokerBlocking = new Invoker(blocking);
        invokerBlocking.invokeCommand();
    }
}

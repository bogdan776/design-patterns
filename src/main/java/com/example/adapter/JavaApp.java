package com.example.adapter;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class JavaApp {

    void saveData() {
        log.info("data saved,");
    }

    void updateData() {
        log.info("data updated");
    }

    void loadData() {
        log.info("data received");
    }

    void deleteData() {
        log.info("data deleted");
    }
}

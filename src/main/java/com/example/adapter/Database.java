package com.example.adapter;

public interface Database {

    void insert();

    void select();

    void update();

    void delete();
}

package com.example.adapter;

public class Adapter extends JavaApp implements Database {

    @Override
    public void insert() {
        saveData();
    }

    @Override
    public void select() {
        loadData();
    }

    @Override
    public void update() {
        updateData();
    }

    @Override
    public void delete() {
        deleteData();
    }
}

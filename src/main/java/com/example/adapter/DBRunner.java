package com.example.adapter;

public class DBRunner {

    public static void main(String[] args) {
        Database database = new Adapter();
        database.insert();
        database.select();
        database.update();
        database.delete();
    }
}

package com.example.templatemethod;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;

/**
 * We implemented a {@link LinkedList} and an {@link ArrayList}
 * with minimal functionality to test the performance.
 * And our method {@link #addAll(Collection)} acts as a template method.
 * As it is implemented in {@link AbstractList#addAll(int, Collection)}, but with one parameter.
 *
 * @param <E> the type of elements in this list
 * @author Bohdan Pereverziev
 */
public interface MyList<E> {

    boolean addAll(Collection<? extends E> collection);

    boolean add(E e);

    E get(int index);

    int size();
}

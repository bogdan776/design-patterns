package com.example.templatemethod;

import java.util.Collection;

public abstract class MyAbstractList<E> implements MyList<E> {

    @Override
    public boolean addAll(Collection<? extends E> collection) {
        for (E e : collection) {
            if (!add(e)) {
                throw new IllegalArgumentException();
            }
        }
        return true;
    }
}

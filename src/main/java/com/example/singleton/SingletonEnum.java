package com.example.singleton;

public enum SingletonEnum {

    INSTANCE;

    private int count;

    public void increment() {
        this.count++;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getCount() {
        return count;
    }
}

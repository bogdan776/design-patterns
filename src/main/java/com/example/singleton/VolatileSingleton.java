package com.example.singleton;

public class VolatileSingleton {

    private static VolatileSingleton instance = null;

    private volatile static boolean instanceCreated = false;

    private static final Object M = new Object();

    private VolatileSingleton() {
        /*
        private constructor
         */
    }

    public VolatileSingleton getInstance() {
        if (!instanceCreated) {
            synchronized (M) {
                try {
                    if (!instanceCreated) {
                        instance = new VolatileSingleton();
                        instanceCreated = true;
                    }
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }
        }
        return instance;
    }
}

package com.example.singleton;

public class LazyInitSingleton {

    private LazyInitSingleton() {
        /*
        private constructor
         */
    }

    /*
    nested class
     */
    private static class SingletonHolder {
        private final static LazyInitSingleton INSTANCE = new LazyInitSingleton();
    }

    public static LazyInitSingleton getInstance() {
        return SingletonHolder.INSTANCE;
    }
}

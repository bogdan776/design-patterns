package com.example.singleton;

import java.util.concurrent.locks.ReentrantLock;

public class LockSingleton {

    private static LockSingleton instance = null;

    private static final ReentrantLock lock = new ReentrantLock();

    private LockSingleton() {
        /*
        private constructor
         */
    }

    public static LockSingleton getInstance() {
        lock.lock();
        try {
            if (instance == null) {
                instance = new LockSingleton();
            }
        } finally {
            lock.unlock();
        }
        return instance;
    }
}

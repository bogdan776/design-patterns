package com.example.singleton.semaphore;

public class SingletonException extends Exception {

    public SingletonException(String message) {
        super(message);
    }
}

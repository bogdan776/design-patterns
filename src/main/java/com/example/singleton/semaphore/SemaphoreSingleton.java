package com.example.singleton.semaphore;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

public class SemaphoreSingleton {

    private static final int MAX_AVAILABLE = 10;

    private static final Semaphore semaphore;

    private static final List<SemaphoreSingleton> instances;

    private String context = "";

    static {
        semaphore = new Semaphore(MAX_AVAILABLE, true);
        instances = new ArrayList<>(MAX_AVAILABLE);
    }

    private SemaphoreSingleton() {
        /*
        private constructor
         */
    }

    public String getContext() {
        return context;
    }

    public void addContext(String context) {
        this.context = this.context.concat(context);
    }

    public static SemaphoreSingleton getInstance(int index) throws SingletonException {
        if (index >= 0 & index < instances.size()) {
            return instances.get(index);
        }
        if (semaphore.tryAcquire()) {
            SemaphoreSingleton singleton = new SemaphoreSingleton();
            instances.add(singleton);
            return singleton;
        }
        throw new SingletonException("Instance limit exceeded");
    }
}

package com.example.singleton;

public class Singleton {

    private static Singleton instance = null;

    private String context = "";

    private Singleton() {
        /*
        private constructor
         */
    }

    public void addContext(String text) {
        this.context = context.concat(text);
    }

    public String getContext() {
        return context;
    }

    public static Singleton getInstance() {
        if (instance == null) {
            instance = new Singleton();
        }
        return instance;
    }
}
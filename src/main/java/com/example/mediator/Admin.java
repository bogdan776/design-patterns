package com.example.mediator;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class Admin implements User {

    private final Chat chat;

    private final String name;

    @Override
    public void sendMessage(String message) {
        chat.sendMessage(this, message);
    }

    @Override
    public void getMessage(String message) {
        System.out.println(this.name + " received message: " + message);
    }
}

package com.example.mediator;

public interface Chat {

    void sendMessage(User user, String message);
}

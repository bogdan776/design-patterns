package com.example.mediator;

import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@Setter
public class TextChat implements Chat {

    private User admin;

    private final Set<User> members = new HashSet<>();

    public void addMemberToChar(User user) {
        members.add(user);
    }

    @Override
    public void sendMessage(User user, String message) {
        for (User u : members) {
            if (!user.equals(u)) {
                u.getMessage(message);
            }
        }
        admin.getMessage(message);
    }
}

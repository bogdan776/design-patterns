package com.example.mediator;

public class Client {

    public static void main(String[] args) {
        TextChat chat = new TextChat();
        User admin = new Admin(chat, "Admin");
        User first = new Member(chat, "First");
        User second = new Member(chat, "Second");
        User third = new Member(chat, "Third");

        chat.setAdmin(admin);
        chat.addMemberToChar(first);
        chat.addMemberToChar(second);
        chat.addMemberToChar(third);

        second.sendMessage("Hello world");
    }
}

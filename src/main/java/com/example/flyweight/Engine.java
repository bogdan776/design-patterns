package com.example.flyweight;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class Engine {

    private int engineDisplacement;

    private int power;

    public void start() {
        log.info("Engine started");
    }

    public void stop() {
        log.info("Engine stopped");
    }
}

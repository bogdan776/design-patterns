package com.example.flyweight;

import com.example.util.Mapper;
import lombok.SneakyThrows;

public interface Vehicle {

    void start();

    void stop();

    @SneakyThrows
    default String info() {
        return Mapper.MAPPER.writeValueAsString(this);
    }
}

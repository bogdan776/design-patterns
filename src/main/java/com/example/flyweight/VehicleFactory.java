package com.example.flyweight;

import com.example.util.Mapper;
import com.example.util.Reader;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

@Log4j2
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class VehicleFactory {

    private final static Map<String, Vehicle> vehicles;

    static {
        vehicles = new HashMap<>();
        init();
    }

    public static Vehicle createVehicle(String model) {
        return vehicles.computeIfAbsent(model, m -> {
            log.debug("Creating new car...");
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                log.error(e.getMessage());
            }
            int engineDisplacement = randomEngineDisplacement();
            int power = randomPower(engineDisplacement);
            Engine engine = new Engine(engineDisplacement, power);
            return new Car(m, engine);
        });
    }

    private static int randomEngineDisplacement() {
        return random(1000, 6000);
    }

    private static int randomPower(int engineDisplacement) {
        if (1000 < engineDisplacement && engineDisplacement < 2000)
            return random(45, 300);
        else if (2000 < engineDisplacement && engineDisplacement < 4000)
            return random(150, 700);
        else return random(500, 1000);
    }

    private static int random(int min, int max) {
        return new Random().ints(min, max).iterator().nextInt();
    }

    @SneakyThrows
    private static void init() {
        log.debug("initialization Map from JSON...");
        String json = Reader.read("input-data/mercedes.json");
        List<Car> vs = new ArrayList<>();
        ObjectMapper mapper = Mapper.MAPPER;
        TypeFactory factory = mapper.getTypeFactory();
        if (vs.addAll(mapper.readValue(json, factory.constructCollectionType(List.class, Car.class))))
            vs.forEach(c -> vehicles.put(c.getModel(), c));
        log.debug("initialization completed");
    }
}

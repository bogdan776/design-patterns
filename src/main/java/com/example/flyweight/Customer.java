package com.example.flyweight;

public class Customer {
    public static void main(String[] args) {
        // Car from JSON
        Vehicle s600 = VehicleFactory.createVehicle("Mercedes-Benz S600");
        s600.start();
        s600.stop();
        System.out.println(s600.info());
        // Random car
        Vehicle unknown = VehicleFactory.createVehicle("BMW 7");
        System.out.println(unknown.info());
        // Try to get random car from Map
        Vehicle bmw7 = VehicleFactory.createVehicle("BMW 7");
        System.out.println(bmw7.info());
        System.out.println(unknown.equals(bmw7));
    }
}

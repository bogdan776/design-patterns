package com.example.bridge;

public interface Developer {

    void writeCode();
}

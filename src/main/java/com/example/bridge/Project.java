package com.example.bridge;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public abstract class Project {

    protected Developer developer;

    public abstract void create();
}

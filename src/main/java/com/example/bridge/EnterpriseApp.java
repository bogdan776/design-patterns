package com.example.bridge;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class EnterpriseApp extends Project {

    public EnterpriseApp(Developer developer) {
        super(developer);
    }

    @Override
    public void create() {
        log.info("We are creating a reliable bank application...");
        developer.writeCode();
    }
}

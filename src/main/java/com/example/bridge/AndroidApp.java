package com.example.bridge;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class AndroidApp extends Project {

    public AndroidApp(Developer developer) {
        super(developer);
    }

    @Override
    public void create() {
        log.info("We are creating cool a app for Xiaomi...");
        developer.writeCode();
    }
}

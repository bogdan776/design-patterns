package com.example.bridge;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class KotlinDeveloper implements Developer {

    @Override
    public void writeCode() {
        log.info("Kotlin developer uses cool syntax");
    }
}

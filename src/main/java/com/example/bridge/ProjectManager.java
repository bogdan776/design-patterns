package com.example.bridge;

public class ProjectManager {

    public static void main(String[] args) {
        Project[] projects = {
                new AndroidApp(new KotlinDeveloper()),
                new AndroidApp(new JavaDeveloper()),
                new EnterpriseApp(new JavaDeveloper())
        };

        for (Project project : projects) {
            project.create();
        }
    }
}

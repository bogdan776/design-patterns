package com.example.bridge;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class JavaDeveloper implements Developer {

    @Override
    public void writeCode() {
        log.info("Java dev has written cool code");
    }
}

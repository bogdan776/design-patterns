package com.example.momento;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@Data
public class JavaProject extends Project {

    private final Set<String> dependencies = new HashSet<>();

    public JavaProject(Float version, String context, String author, String... dependencies) {
        super(version, context, author);
        this.dependencies.addAll(Arrays.asList(dependencies));
    }

    @Override
    public String toString() {
        return super.toString();
    }
}

package com.example.momento;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Momento {

    private final Float version;

    private final String context;
}

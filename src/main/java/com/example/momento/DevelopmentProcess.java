package com.example.momento;

public class DevelopmentProcess {

    public static void main(String[] args) {
        Project project = new JavaProject(1.0f, "Good Java code", "Bogdan",
                "lombok", "mysql-connector", "junit");
        Repository repository = new Repository();
        log("Version 1.0");
        log(project.toString());
        log("Commit");
        repository.push(project.commit());

        log("Update project to version 1.1");
        project.setVersion(1.1f);
        project.setContext("Not bad Java code");
        log(project.toString());
        log("Commit");
        repository.push(project.commit());

        log("Update project to version 1.2");
        project.setVersion(1.2f);
        project.setContext("Not good Java code");
        log(project.toString());
        log("Commit");
        repository.push(project.commit());

        log("All versions in repo:");
        repository.log();

        log("Project died");
        log("Rollback to version 1.1");
        project.rollback(repository.revert(1.1f));
        log(project.toString());

        log("Rollback to version 1.0");
        project.rollback(repository.revert(1.0f));
        log(project.toString());
    }

    private static void log(String message) {
        System.out.println(message);
    }
}

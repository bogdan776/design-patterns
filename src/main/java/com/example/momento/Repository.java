package com.example.momento;

import java.util.HashMap;
import java.util.Map;

public class Repository {

    private final Map<Float, Momento> versions = new HashMap<>();

    public void push(Momento momento) {
        versions.put(momento.getVersion(), momento);
    }

    public Momento revert(Float version) {
        if (versions.containsKey(version)) {
            return versions.get(version);
        } else throw new NullPointerException();
    }

    public void reset(Float version) {
        if (versions.containsKey(version)) {
            versions.remove(version);
        } else throw new NullPointerException();
    }

    public void resetAll() {
        versions.clear();
    }

    public void log() {
        versions.forEach((k, v) -> System.out.println("Version: " + k + " Project" + v));
    }
}

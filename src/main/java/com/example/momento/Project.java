package com.example.momento;

import com.example.util.Mapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Project {

    private Float version;

    private String context;

    private String author;

    public Momento commit() {
        return new Momento(version, context);
    }

    public void rollback(Momento momento) {
        this.version = momento.getVersion();
        this.context = momento.getContext();
    }

    @SneakyThrows
    @Override
    public String toString() {
        return Mapper.MAPPER.writeValueAsString(this);
    }
}

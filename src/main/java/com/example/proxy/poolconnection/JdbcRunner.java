package com.example.proxy.poolconnection;

import com.example.proxy.poolconnection.sql.MyConnection;
import com.example.proxy.poolconnection.sql.MyPreparedStatement;
import com.example.proxy.poolconnection.sql.MyResultSet;

public class JdbcRunner {

    public static void main(String[] args) throws InterruptedException {
        ConnectionPool connectionPool = new ConnectionPool(5);
        MyConnection connection = connectionPool.getConnection();
        System.out.println(connection.getClass().getSimpleName()); // ProxyConnection
        connection.setAutoCommit(false);
        MyPreparedStatement preparedStatement = connection.prepareStatement("select * from table where id=?");
        preparedStatement.setInt(1, 154);
        MyResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next())
            System.out.println(resultSet.getString());
        if (!resultSet.isClosed()) {
            resultSet.close();
        }
        if (!preparedStatement.isClosed()) {
            preparedStatement.close();
        }
        connectionPool.closeConnection(connection);
    }
}

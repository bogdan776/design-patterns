package com.example.proxy.poolconnection.connector;

import com.example.proxy.poolconnection.sql.MyResultSet;
import com.example.proxy.poolconnection.sql.MyStatement;
import lombok.extern.log4j.Log4j2;

import java.util.Random;

@Log4j2
public class StatementImpl implements MyStatement {

    private boolean closed = false;

    private final Random random = new Random();

    @Override
    public MyResultSet executeQuery(String sql) {
        return new ResultSetImpl();
    }

    @Override
    public int executeUpdate(String sql) {
        return random.nextInt();
    }

    @Override
    public void close() {
        log.info("Statement is closed");
        closed = true;
    }

    @Override
    public boolean isClosed() {
        return closed;
    }
}

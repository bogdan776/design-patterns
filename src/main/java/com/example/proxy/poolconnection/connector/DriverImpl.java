package com.example.proxy.poolconnection.connector;

import com.example.proxy.poolconnection.sql.MyConnection;
import com.example.proxy.poolconnection.sql.MyDriver;
import com.example.proxy.poolconnection.sql.MyDriverManager;

import java.util.Properties;

public class DriverImpl implements MyDriver {

    static {
        MyDriverManager.registerDriver(new DriverImpl());
    }

    @Override
    public MyConnection connect(String url, Properties info) {
        return new ConnectionImpl(url, info);
    }
}

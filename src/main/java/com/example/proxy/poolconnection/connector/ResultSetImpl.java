package com.example.proxy.poolconnection.connector;

import com.example.proxy.poolconnection.sql.MyResultSet;
import lombok.extern.log4j.Log4j2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

@Log4j2
public class ResultSetImpl implements MyResultSet {

    private final List<String> list = new ArrayList<>(Arrays.asList("One", "Two", "Three"));
    private final Iterator<String> iterator = list.iterator();

    private boolean closed = false;

    @Override
    public boolean next() {
        return iterator.hasNext();
    }

    @Override
    public String getString() {
        return iterator.next();
    }

    @Override
    public void close() {
        log.info("ResultSet is closed");
        closed = true;
    }

    @Override
    public boolean isClosed() {
        return closed;
    }
}

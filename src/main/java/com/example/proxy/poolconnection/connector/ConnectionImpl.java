package com.example.proxy.poolconnection.connector;

import com.example.proxy.poolconnection.sql.MyConnection;
import com.example.proxy.poolconnection.sql.MyPreparedStatement;
import com.example.proxy.poolconnection.sql.MyStatement;
import lombok.Data;
import lombok.extern.log4j.Log4j2;

import java.util.Properties;

@Data
@Log4j2
public class ConnectionImpl implements MyConnection {

    private final String url;

    private final Properties info;

    private boolean closed;

    private boolean autoCommit = true;

    public ConnectionImpl(String url, Properties info) {
        this.url = url;
        this.info = info;
    }

    @Override
    public MyStatement createStatement() {
        log.info("Statement created");
        return new StatementImpl();
    }

    @Override
    public MyPreparedStatement prepareStatement(String sql) {
        log.info("PreparedStatement created");
        return new PreparedStatementImpl();
    }

    @Override
    public void setAutoCommit(boolean autoCommit) {
        log.info("auto commit: " + autoCommit);
        this.autoCommit = autoCommit;
    }

    @Override
    public void commit() {
        log.info("committed");
    }

    @Override
    public void rollback() {
        log.info("rollback");
    }

    @Override
    public void close() {
        log.info("connection closed");
        closed = true;
    }

    @Override
    public boolean isClosed() {
        return closed;
    }
}

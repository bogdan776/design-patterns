package com.example.proxy.poolconnection.connector;

import com.example.proxy.poolconnection.sql.MyPreparedStatement;
import com.example.proxy.poolconnection.sql.MyResultSet;
import lombok.extern.log4j.Log4j2;

import java.util.Random;

@Log4j2
public class PreparedStatementImpl implements MyPreparedStatement {

    private boolean closed = false;

    private final Random random = new Random();

    @Override
    public MyResultSet executeQuery() {
        log.info("execute query and returned ResultSet");
        return new ResultSetImpl();
    }

    @Override
    public int executeUpdate() {
        log.info("table updated");
        return random.nextInt();
    }

    @Override
    public void setInt(int parameterIndex, int x) {
        log.info("parameter index=" + parameterIndex + ", value=" + x);
    }

    @Override
    public void setString(int parameterIndex, String x) {
        log.info("parameter index=" + parameterIndex + ", value=" + x);
    }

    @Override
    public boolean execute() {
        boolean res = random.nextBoolean();
        log.info("result: " + res);
        return res;
    }

    @Override
    public void close() {
        log.info("PreparedStatement is closed");
        closed = true;
    }

    @Override
    public boolean isClosed() {
        return closed;
    }
}

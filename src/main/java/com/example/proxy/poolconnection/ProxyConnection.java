package com.example.proxy.poolconnection;

import com.example.proxy.poolconnection.sql.MyConnection;
import com.example.proxy.poolconnection.sql.MyPreparedStatement;
import com.example.proxy.poolconnection.sql.MyStatement;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;

import java.sql.SQLException;

@AllArgsConstructor(access = AccessLevel.PACKAGE)
public class ProxyConnection implements MyConnection {

    private final MyConnection connection;

    @Override
    public MyStatement createStatement() throws SQLException {
        return connection.createStatement();
    }

    @Override
    public MyPreparedStatement prepareStatement(String sql) {
        return connection.prepareStatement(sql);
    }

    @Override
    public void setAutoCommit(boolean autoCommit) {
        connection.setAutoCommit(autoCommit);
    }

    @Override
    public void commit() {
        connection.commit();
    }

    @Override
    public void rollback() {
        connection.rollback();
    }

    @Override
    public void close() {
        connection.close();
    }

    @Override
    public boolean isClosed() {
        return connection.isClosed();
    }
}

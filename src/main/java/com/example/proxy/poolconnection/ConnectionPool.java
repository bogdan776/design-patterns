package com.example.proxy.poolconnection;

import com.example.proxy.poolconnection.sql.MyConnection;
import com.example.proxy.poolconnection.sql.MyDriverManager;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;

import java.util.Properties;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

@Log4j2
public class ConnectionPool {

    private final BlockingQueue<MyConnection> connectionQueue;

    public ConnectionPool(final int POOL_SIZE) {
        connectionQueue = new ArrayBlockingQueue<>(POOL_SIZE);
        initDriver();
        for (int i = 0; i < POOL_SIZE; i++) {
            MyConnection connection = createConnection();
            connectionQueue.offer(connection);
        }
    }

    public MyConnection getConnection() throws InterruptedException {
        MyConnection connection;
        connection = connectionQueue.take();
        return new ProxyConnection(connection);
    }

    public void closeConnection(MyConnection connection) {
        log.info("connection returned to the pool");
        connectionQueue.offer(connection);
    }

    @SneakyThrows
    private MyConnection createConnection() {
        return MyDriverManager.getConnection("jdbc:sql://127.0.0.1:3306/project", info());
    }

    private void initDriver() {
        try {
            Class.forName("com.example.proxy.poolconnection.connector.DriverImpl");
        } catch (ClassNotFoundException e) {
            log.error("driver not found. Ex msg: " + e);
        }
    }

    private Properties info() {
        Properties properties = new Properties();
        properties.put("user", "root");
        properties.put("password", "admin");
        properties.put("characterEncoding", "UTF-8");
        return properties;
    }
}

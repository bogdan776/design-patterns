package com.example.proxy.poolconnection.sql;

public interface MyResultSet {

    boolean next();

    String getString();

    void close();

    boolean isClosed();
}

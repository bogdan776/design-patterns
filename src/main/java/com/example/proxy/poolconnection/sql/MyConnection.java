package com.example.proxy.poolconnection.sql;

import java.sql.SQLException;

public interface MyConnection {

    MyStatement createStatement() throws SQLException;

    MyPreparedStatement prepareStatement(String sql);

    void setAutoCommit(boolean autoCommit);

    void commit();

    void rollback();

    void close();

    boolean isClosed();
}

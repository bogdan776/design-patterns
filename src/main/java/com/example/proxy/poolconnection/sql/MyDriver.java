package com.example.proxy.poolconnection.sql;

import java.util.Properties;

public interface MyDriver {

    MyConnection connect(String url, Properties info);
}

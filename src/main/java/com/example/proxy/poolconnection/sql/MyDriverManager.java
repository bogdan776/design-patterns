package com.example.proxy.poolconnection.sql;

import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;

import java.sql.SQLException;
import java.util.Properties;
import java.util.concurrent.CopyOnWriteArrayList;

@Log4j2
public class MyDriverManager {

    private final static CopyOnWriteArrayList<DriverInfo> registeredDrivers = new CopyOnWriteArrayList<>();

    public static MyConnection getConnection(String url, Properties info) throws SQLException {
        log.info("getting connection");
        for (DriverInfo driverInfo : registeredDrivers) {
            MyConnection connection = driverInfo.driver.connect(url, info);
            if (connection != null)
                return connection;
        }
        throw new SQLException("no suitable driver found for " + url);
    }

    public static void registerDriver(MyDriver driver) {
        if (driver != null)
            if (registeredDrivers.addIfAbsent(new DriverInfo(driver)))
                log.info("driver registered");
            else log.error("driver is not registered");
        else throw new NullPointerException();
    }
}

@AllArgsConstructor
class DriverInfo {

    MyDriver driver;
}

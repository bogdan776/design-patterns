package com.example.proxy.poolconnection.sql;

public interface MyStatement {

    MyResultSet executeQuery(String sql);

    int executeUpdate(String sql);

    void close();

    boolean isClosed();
}

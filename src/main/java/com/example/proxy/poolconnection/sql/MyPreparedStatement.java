package com.example.proxy.poolconnection.sql;

public interface MyPreparedStatement {

    MyResultSet executeQuery();

    int executeUpdate();

    void setInt(int parameterIndex, int x);

    void setString(int parameterIndex, String x);

    boolean execute();

    void close();

    boolean isClosed();
}

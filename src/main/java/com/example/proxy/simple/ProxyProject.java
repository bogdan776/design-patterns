package com.example.proxy.simple;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ProxyProject implements Project {

    private final String url;

    private Project project;

    @Override
    public void run() {
        if (project == null)
            project = new JavaProject(url);
        project.run();
    }
}

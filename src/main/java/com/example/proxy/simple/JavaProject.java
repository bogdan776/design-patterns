package com.example.proxy.simple;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class JavaProject implements Project {

    private final String url;

    public JavaProject(String url) {
        this.url = url;
        load();
    }

    public void load() {
        log.info("Loading project from " + url);
    }

    @Override
    public void run() {
        log.info("Running project...");
    }
}

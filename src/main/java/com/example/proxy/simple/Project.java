package com.example.proxy.simple;

public interface Project {

    void run();
}

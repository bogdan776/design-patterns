package com.example.proxy.simple;

public class ProjectRunner {

    public static void main(String[] args) {
        Project project = new ProxyProject("https://gitlab.com/bogdan776/java-project");
        project.run();
    }
}

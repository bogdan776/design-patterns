package com.example.facade;

import com.example.facade.system.AirFlowController;
import com.example.facade.system.AirFlowMeter;
import com.example.facade.system.CatalyticConverter;
import com.example.facade.system.Clutch;
import com.example.facade.system.CoolingController;
import com.example.facade.system.FuelInjector;
import com.example.facade.system.FuelPump;
import com.example.facade.system.IgnitionSystem;
import com.example.facade.system.Radiator;
import com.example.facade.system.Starter;
import com.example.facade.system.TemperatureSensor;
import com.example.facade.system.Transmission;

public class CarEngine {

    private static final Integer DEFAULT_COOLING_TEMPERATURE = 90;
    private static final Integer MAX_ALLOWED_TEMPERATURE = 50;
    private final IgnitionSystem ignitionSystem = new IgnitionSystem();
    private final FuelPump fuelPump = new FuelPump();
    private final FuelInjector fuelInjector = new FuelInjector(fuelPump);
    private final AirFlowMeter meter = new AirFlowMeter();
    private final AirFlowController airFlowController = new AirFlowController(meter);
    private final Clutch clutch = new Clutch();
    private final Transmission transmission = new Transmission();
    private final Starter starter = new Starter();
    private final Radiator radiator = new Radiator();
    private final TemperatureSensor sensor = new TemperatureSensor();
    private final CoolingController coolingController = new CoolingController(radiator, sensor);
    private final CatalyticConverter catalyticConverter = new CatalyticConverter();

    public void startEngine() {
        ignitionSystem.on();
        airFlowController.takeAir();
        fuelInjector.on();
        fuelInjector.inject();
        clutch.on();
        transmission.n();
        clutch.off();
        starter.start();
        coolingController.setTemperatureUpperLimit(DEFAULT_COOLING_TEMPERATURE);
        coolingController.run();
        catalyticConverter.on();
    }

    public void stopEngine() {
        clutch.on();
        transmission.n();
        clutch.off();
        ignitionSystem.off();
        fuelInjector.off();
        catalyticConverter.off();
        coolingController.cool(MAX_ALLOWED_TEMPERATURE);
        coolingController.stop();
        airFlowController.off();
        clutch.on();
        transmission.one();
        clutch.off();
        transmission.parking();
    }
}

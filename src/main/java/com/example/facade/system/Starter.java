package com.example.facade.system;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Setter(AccessLevel.PRIVATE)
@Getter
@Log4j2
public class Starter {

    private StarterState state;

    public Starter() {
        setState(StarterState.NON);
    }

    public void start() {
        log.info("Starting...");
        setState(StarterState.START);
    }

    private enum StarterState {
        START, NON
    }
}

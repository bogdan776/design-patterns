package com.example.facade.system;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class Radiator {

    public void on() {
        log.info("Radiator switched on!");
    }

    public void off() {
        log.info("Radiator switched off!");
    }

    public void setSpeed(int speed) {
        log.info("Setting radiator speed to {}", speed);
    }
}

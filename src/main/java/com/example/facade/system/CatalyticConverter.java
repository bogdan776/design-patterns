package com.example.facade.system;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class CatalyticConverter {

    public void on() {
        log.info("Catalytic Converter switched on!");
    }

    public void off() {
        log.info("Catalytic Converter switched off!");
    }
}

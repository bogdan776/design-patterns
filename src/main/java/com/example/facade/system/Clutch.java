package com.example.facade.system;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Setter(AccessLevel.PRIVATE)
@Getter
@Log4j2
public class Clutch {

    private boolean engageClutch = false;

    public void on() {
        log.info("Clutch switched on.");
        setEngageClutch(true);
    }

    public void off() {
        log.info("Clutch switched off.");
        setEngageClutch(false);
    }
}

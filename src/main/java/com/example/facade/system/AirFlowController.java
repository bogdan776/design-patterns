package com.example.facade.system;

import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;

@Log4j2
@AllArgsConstructor
public class AirFlowController {

    private final AirFlowMeter meter;

    public void takeAir() {
        meter.getMeasurements();
        log.info("Air provided!");
    }

    public void off() {
        log.info("Air controller switched off.");
    }
}

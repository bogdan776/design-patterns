package com.example.facade.system;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

@Log4j2
@RequiredArgsConstructor
@Getter
public class CoolingController {

    private static final int DEFAULT_RADIATOR_SPEED = 10;

    private int temperatureUpperLimit;
    private final Radiator radiator;
    private final TemperatureSensor temperatureSensor;

    public void run() {
        log.info("Cooling Controller ready!");
        radiator.setSpeed(DEFAULT_RADIATOR_SPEED);
    }

    public void cool(Integer maxAllowedTemp) {
        log.info("Scheduled cooling with maximum allowed temperature {}", maxAllowedTemp);
        temperatureSensor.getTemperature();
        radiator.on();
    }

    public void stop() {
        log.info("Stopping Cooling Controller..");
        radiator.off();
    }

    public void setTemperatureUpperLimit(Integer temperatureUpperLimit) {
        log.info("Setting temperature upper limit to {}", temperatureUpperLimit);
        this.temperatureUpperLimit = temperatureUpperLimit;
    }
}

package com.example.facade.system;

import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;

@Log4j2
@AllArgsConstructor
public class FuelInjector {

    private final FuelPump fuelPump;

    public void on() {
        log.info("Fuel injector ready to inject fuel.");
    }

    public void inject() {
        fuelPump.pump();
        log.info("Fuel injected.");
    }

    public void off() {
        log.info("Stopping Fuel injector..");
    }
}

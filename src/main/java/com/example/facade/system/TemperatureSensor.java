package com.example.facade.system;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class TemperatureSensor {

    public void getTemperature(){
        log.info("Getting temperature from the sensor...");
    }
}

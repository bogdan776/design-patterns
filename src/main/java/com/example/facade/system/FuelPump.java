package com.example.facade.system;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class FuelPump {

    public void pump() {
        log.info("Fuel Pump is pumping fuel..");
    }
}

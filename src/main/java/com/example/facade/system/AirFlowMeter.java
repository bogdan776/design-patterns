package com.example.facade.system;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class AirFlowMeter {

    public void getMeasurements() {
        log.info("Getting air measurements..");
    }
}

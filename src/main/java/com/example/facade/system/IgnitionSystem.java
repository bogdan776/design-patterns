package com.example.facade.system;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class IgnitionSystem {

    public void on() {
        log.info("Ignition on.");
    }

    public void off() {
        log.info("Ignition off.");
    }
}

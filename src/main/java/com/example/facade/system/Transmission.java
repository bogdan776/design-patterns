package com.example.facade.system;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Getter
@Setter(AccessLevel.PRIVATE)
@Log4j2
public class Transmission {

    private Gear gear;

    public Transmission() {
        setGear(Gear.PARKING);
    }

    public void n() {
        log.info("Neutral gear engaged");
        setGear(Gear.NEUTRAL);
    }

    public void one() {
        log.info("First gear engaged");
        setGear(Gear.FIRST);
    }

    public void two() {
        log.info("Second gear engaged");
        setGear(Gear.SECOND);
    }

    public void three() {
        log.info("Third gear engaged");
        setGear(Gear.THIRD);
    }

    public void four() {
        log.info("Fourth gear engaged");
        setGear(Gear.FOURTH);
    }

    public void five() {
        log.info("Fifth gear engaged");
        setGear(Gear.FIFTH);
    }

    public void r() {
        log.info("Reverse gear engaged");
        setGear(Gear.REVERSE);
    }

    public void parking() {
        log.info("Parking engaged");
        setGear(Gear.PARKING);
    }

    private enum Gear {
        NEUTRAL, FIRST, SECOND, THIRD, FOURTH, FIFTH, REVERSE, PARKING
    }
}

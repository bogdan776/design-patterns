package com.example.facade;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class CarEngineDemo {

    public static void main(String[] args) {
        CarEngine engine = new CarEngine();
        log.info("START ENGINE");
        engine.startEngine();
        log.info("STOP ENGINE");
        engine.stopEngine();
    }
}

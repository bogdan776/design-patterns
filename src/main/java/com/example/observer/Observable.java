package com.example.observer;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Bohdan Pereverziev
 * @see java.util.Observable
 * This class has unimportant differences.
 * Non-synchronized methods and advanced features
 */
public class Observable {

    private boolean changed = false;

    private final List<Observer> obs;

    public Observable() {
        obs = new ArrayList<>();
    }

    public void addObserver(Observer observer) {
        if (observer == null)
            throw new NullPointerException();
        if (!obs.contains(observer)) {
            obs.add(observer);
        }
    }

    public void notifyObservers(String... args) {
        notifyObservers(false, args);
    }

    public void notifyObservers(boolean delete, String... args) {
        if (!hasChanged()) {
            return;
        }
        Object[] arrLocal = obs.toArray();
        clearChanged();
        for (Object o : arrLocal) {
            if (delete) {
                ((Observer) o).delete(this, args[0]);
            } else {
                ((Observer) o).update(this, args);
            }
        }
    }

    public void deleteObserver(Observer observer) {
        obs.remove(observer);
    }

    public void deleteObservers() {
        obs.clear();
    }

    protected void setChanged() {
        changed = true;
    }

    protected void clearChanged() {
        changed = false;
    }

    public boolean hasChanged() {
        return changed;
    }

    public int countObservers() {
        return obs.size();
    }
}

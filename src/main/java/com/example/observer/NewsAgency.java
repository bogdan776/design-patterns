package com.example.observer;

import lombok.Getter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Getter
public class NewsAgency extends Observable {

    private final List<String> news = new ArrayList<>();

    public void addNews(String... news) {
        this.news.addAll(Arrays.asList(news));
        setChanged();
        notifyObservers(news);
    }

    public void removeNews(String removedNews) {
        if (news.contains(removedNews)) {
            news.remove(removedNews);
        } else throw new NullPointerException();
        setChanged();
        notifyObservers(true, removedNews);
    }
}

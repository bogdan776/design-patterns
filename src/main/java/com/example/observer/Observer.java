package com.example.observer;

/**
 * @author Bohdan Pereverziev
 * @see java.util.Observer
 */
public interface Observer {

    void update(Observable observable, String... args);

    default void delete(Observable observable, String arg) {
        /*
        Method declared to simplify the implementation of some functionality.
         */
    }
}

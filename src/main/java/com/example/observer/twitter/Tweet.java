package com.example.observer.twitter;

import lombok.Data;

import java.util.Date;

@Data
public class Tweet {

    private String author;

    private String context;

    private Date date;

    public Tweet(String author, String context) {
        this.author = author;
        this.context = context;
        date = new Date();
    }

    @Override
    public String toString() {
        String separator = System.lineSeparator();
        return "Author: " + author + separator +
                context + separator +
                "Post published: " + date.toString() + separator +
                "---------------------------------------------" + separator;
    }
}

package com.example.observer.twitter;

public class TwitterRunner {

    public static void main(String[] args) throws InterruptedException {
        Twitter michaelJackson = new Twitter("Michael Jackson");
        Twitter elvisPresley = new Twitter("Elvis Presley");
        Twitter kherSGory = new Twitter("Very popular pop singer");

        Follower me = new Follower("Bogdan");
        Follower notMe = new Follower("Not Bogdan");

        michaelJackson.addObserver(me);
        michaelJackson.addObserver(notMe);
        elvisPresley.addObserver(me);
        kherSGory.addObserver(notMe);

        log("-------- [ MAIN PAGE ] -------");
        michaelJackson.tweet("Hi, I'm alive! Ha-ha it is a joke");
        Thread.sleep(1500);
        michaelJackson.tweet("=(");
        Thread.sleep(2000);
        elvisPresley.tweet("Always on my mind");
        Thread.sleep(3000);
        kherSGory.tweet("Another smart post");
        Thread.sleep(1000);
        kherSGory.tweet("Next philosophical post");
        Thread.sleep(1000);
        log("-------- [ MICHAEL JACKSON`S PAGE ] -------");
        log(michaelJackson.toString());
        log("-------- [ ALL TWEETS ] -------");
        me.allTweets();
    }

    private static void log(String text) {
        System.out.println(text);
    }
}

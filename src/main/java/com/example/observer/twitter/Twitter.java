package com.example.observer.twitter;

import com.example.observer.Observable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class Twitter extends Observable {

    private final List<Tweet> tweets = new ArrayList<>();

    private String name;

    public void tweet(String text) {
        Tweet tweet = new Tweet(name, text);
        tweets.add(tweet);
        setChanged();
        notifyObservers(tweet.toString());
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (!tweets.isEmpty()) {
            sb.append(name).append("'s tweets:").append(System.lineSeparator())
                    .append(System.lineSeparator());
            for (Tweet tweet : tweets) {
                sb.append(tweet.getContext()).append(System.lineSeparator())
                        .append("Post published: ").append(tweet.getDate().toString())
                        .append(System.lineSeparator())
                        .append("---------------------------------------------")
                        .append(System.lineSeparator());
            }
        } else sb.append(name).append(" has not posted any posts yet.").append(System.lineSeparator());
        return sb.toString();
    }
}

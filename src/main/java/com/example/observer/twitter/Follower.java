package com.example.observer.twitter;

import com.example.observer.Observable;
import com.example.observer.Observer;
import lombok.Data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Data
public class Follower implements Observer {

    private String name;

    private final List<String> tweets = new ArrayList<>();

    public Follower(String name) {
        this.name = name;
    }

    @Override
    public void update(Observable observable, String... args) {
        tweets.addAll(Arrays.asList(args));
        print("Hello, " + name + "!");
        print("[New post]");
        print(args[0]);
    }

    public void allTweets() {
        tweets.forEach(this::print);
    }

    private void print(String text) {
        System.out.println(text);
    }
}

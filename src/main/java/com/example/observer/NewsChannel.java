package com.example.observer;

import lombok.Getter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Getter
public class NewsChannel implements Observer {

    private final List<String> news = new ArrayList<>();

    @Override
    public void update(Observable observable, String... args) {
        this.news.addAll(Arrays.asList(args));
    }

    @Override
    public void delete(Observable observable, String arg) {
        if (news.contains(arg)) {
            news.remove(arg);
        } else throw new NullPointerException();
    }

    @Override
    public String toString() {
        StringBuilder sBuilder = new StringBuilder();
        if (!news.isEmpty()) {
            sBuilder.append("News:").append(System.lineSeparator());
            int i = 0;
            for (String n : news) {
                sBuilder.append(++i).append(". ")
                        .append(n).append(System.lineSeparator());
            }
        } else sBuilder.append("No news yet");
        return sBuilder.toString();
    }
}

package com.example.strategy;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Cat {

    private PetActivity activity;

    public void executeActivity() {
        activity.execute();
    }
}

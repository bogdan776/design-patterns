package com.example.strategy;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class Sleeping implements PetActivity {

    @Override
    public void execute() {
        log.info("It's time to go to sleep... Although what else am I doing?");
    }
}

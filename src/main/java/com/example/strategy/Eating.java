package com.example.strategy;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class Eating implements PetActivity {

    @Override
    public void execute() {
        log.info("Fish! Fish! Fish! But this flower looks delicious");
    }
}

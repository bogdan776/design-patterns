package com.example.strategy;

public class CatOwnerDemo {

    public static void main(String[] args) {
        Cat cat = new Cat(new Playing());
        cat.executeActivity();
        cat.setActivity(new Eating());
        cat.executeActivity();
        cat.setActivity(new Sleeping());
        cat.executeActivity();
    }
}

package com.example.strategy;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class Playing implements PetActivity {

    @Override
    public void execute() {
        log.info("Play with me man! But do not touch me!");
    }
}

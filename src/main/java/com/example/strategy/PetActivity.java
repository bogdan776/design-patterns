package com.example.strategy;

public interface PetActivity {

    void execute();
}
